<?php
namespace vegcoders\core\basic;

class BasicLogged extends BasicSingleLogged
{
	protected static $_instance;
	protected static $_can_debug;
	protected static $_can_debug_collect;

	public static function log($str, $color = '') {
		return self::_log($str, $color);
	}
}
class SecondaryLogged extends BasicLogged
{
	protected static $_instance;
	protected static $_can_debug;
	protected static $_can_debug_collect;
}

class BasicSingleLoggedTest extends \PHPUnit_Framework_TestCase
{

	public function testBasicSingleLogged()
	{
		$first = 'first value of basic single logged';
		$second = 'second value of basic single logged';
		$third = 'third value of basic single logged';

		//независимые параметри дебага
		SecondaryLogged::setDebug(false);
		BasicLogged::setDebug(true);
		$this->assertEquals(false, SecondaryLogged::getDebug());
		$this->assertEquals(true, BasicLogged::getDebug());

		//независимое логгирование
		BasicLogged::log($first);
		SecondaryLogged::log($first);
		$this->assertEquals($first . '<br/>', BasicLogged::debug());
		$this->assertEquals('Turn on if DEBUG_COLLECT', SecondaryLogged::debug());
		$this->assertEquals(false, SecondaryLogged::fullDebug());

		//включили второй
		SecondaryLogged::setDebug(true);
		BasicLogged::log($second);
		SecondaryLogged::log($second);
		$this->assertEquals($second . '<br/>', SecondaryLogged::debug());
		$this->assertEquals($second . '<br/>', BasicLogged::debug());
		$this->assertEquals(false, BasicLogged::debug());
		$this->assertEquals(false, SecondaryLogged::debug());

		//после очистки ничего не остается, соседний не затронут
		BasicLogged::clearDebug();
		$this->assertEquals(false, BasicLogged::fullDebug());
		$this->assertEquals('second value of basic single logged<br/>', SecondaryLogged::fullDebug());

		//после очистки все еще можно добавить запись
		BasicLogged::clearDebug();
		BasicLogged::log($second);
		$this->assertEquals($second . '<br/>', BasicLogged::debug());

		//если был отключен на время - то записи до отключения видим
		$this->assertEquals(true, BasicLogged::log($third, 'blue'));
		BasicLogged::setDebug(false);
		$this->assertEquals(false, BasicLogged::log($second, 'blue'));
		BasicLogged::setDebug(true);
		$this->assertEquals('<div style="color:blue">' . $third . '</div><br/>', BasicLogged::debug());
	}
}
