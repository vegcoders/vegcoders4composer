<?php
namespace vegcoders\core\basic;

use vegcoders\core\cache\Cache;
use vegcoders\core\db\DB;
use vegcoders\core\debug\Debug;
use vegcoders\core\mail\Mail;

class BasicSingleLoggedChildsTest extends \PHPUnit_Framework_TestCase
{

	public function testBasicSingleLoggedChilds()
	{		
		Cache::setDebug(true);
		DB::setDebug(true);
		Debug::setDebug(true);
		Mail::setDebug(true);

		Cache::setDebug(false);
		$this->assertEquals(false, Cache::getDebug());
		$this->assertEquals(true, DB::getDebug());
		$this->assertEquals(true, Debug::getDebug());
		$this->assertEquals(true, Mail::getDebug());
		Cache::setDebug(true);

		DB::setDebug(false);
		$this->assertEquals(true, Cache::getDebug());
		$this->assertEquals(false, DB::getDebug());
		$this->assertEquals(true, Debug::getDebug());
		$this->assertEquals(true, Mail::getDebug());
		DB::setDebug(true);

		Debug::setDebug(false);
		$this->assertEquals(true, Cache::getDebug());
		$this->assertEquals(true, DB::getDebug());
		$this->assertEquals(false, Debug::getDebug());
		$this->assertEquals(true, Mail::getDebug());
		Debug::setDebug(true);

		Mail::setDebug(false);
		$this->assertEquals(true, Cache::getDebug());
		$this->assertEquals(true, DB::getDebug());
		$this->assertEquals(true, Debug::getDebug());
		$this->assertEquals(false, Mail::getDebug());
		Mail::setDebug(true);
	}
}