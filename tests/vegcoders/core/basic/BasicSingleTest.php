<?php
namespace vegcoders\core\basic;

class Basic extends BasicSingle
{
	protected static $_instance;
	public $value = 'basic';
	public static function setValue($val) {
		$object = self::getInstance();
		$object->value = $val;
	}
	public static function getValue() {
		$object = self::getInstance();
		return $object->value;
	}
}
class Secondary extends Basic
{
	protected static $_instance;
	public $value = 'second';
	public $other_value = 'afterConstruction value';
	public function _afterConstruction($params)
	{
		$this->other_value  = $params;
		return parent::_afterConstruction($params);
	}
	public static function otherValue()
	{
		if (!self::checkIfCreated()) return false;
		return self::$_instance->other_value;
	}

}

class BasicSingleTest extends \PHPUnit_Framework_TestCase
{

	public function testBasicSingle()
	{
		$this->assertEquals(false, Secondary::otherValue());


		$first = 'first value of basic single';
		$second = 'second value of basic single';
		Basic::setValue($first);
		Secondary::setValue($second);

		$this->assertEquals(array(), Secondary::otherValue());

		$this->assertEquals($first, Basic::getValue());
		$this->assertEquals($second, Secondary::getValue());
		
		Secondary::setValue($first);
		Basic::setValue($second);
		$this->assertEquals($second, Basic::getValue());
		$this->assertEquals($first, Secondary::getValue());
	}
}
