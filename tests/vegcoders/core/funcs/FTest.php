<?php
use vegcoders\core\funcs\F;


class FTest extends \PHPUnit_Framework_TestCase
{
	public function testGetDefault()
	{
		$var = 'test';
		$val = 1;
		F::setGetArray(array($var => $val));

		$this->assertEquals($val, F::get($var));
		$this->assertEquals($val, F::get($var, VEG_PARAM_INT));
		$this->assertEquals($val, F::get($var, VEG_PARAM_INT, 22));
		$this->assertEquals(array($val), F::get($var, VEG_PARAM_ARRAY));
		$this->assertEquals($val, F::get($var, VEG_PARAM_INARRAY, array(1,2)));
		$this->assertEquals(3, F::get($var, VEG_PARAM_INARRAY, array(3,2)));
		$this->assertEquals(array($val => $val), F::get($var, VEG_PARAM_INTS));
		$this->assertEquals(false, F::get($var, VEG_PARAM_DATE));
		$this->assertEquals(false, F::get($var, VEG_PARAM_DATETIME));
		$this->assertEquals(false, F::get($var, VEG_PARAM_EMAIL));
		$this->assertEquals($val, F::get($var, VEG_PARAM_PHONE));
		$this->assertEquals($val, F::get($var, VEG_PARAM_EDIT));
		try {
			$this->assertEquals('no error!', F::get($var, 'sometypesdfgsdfg'));
		} catch (\Exception $e) {
			$this->assertEquals('F::get type is undefined sometypesdfgsdfg', $e->getMessage());
		}
	}

	public function testGetAll()
	{
		$var = 'test';
		$val = 'adgadfgsdfg,1,333,<html>\'dfgsdgsdfgsdfgsdfg</html>';
		F::setGetArray(array($var => $val));
		$this->assertEquals('adgadfgsdfg,1,333,\'dfgsdgsdfgsdfgsdfg', F::get($var));

		$this->assertEquals(false, F::get($var, VEG_PARAM_INT));
		$this->assertEquals(33, F::get($var, VEG_PARAM_INT, 33));
		$this->assertEquals(false, F::get($var, VEG_PARAM_INT, 'jk;jkl;'));

		$this->assertEquals(array('adgadfgsdfg,1,333,\'dfgsdgsdfgsdfgsdfg'), F::get($var, VEG_PARAM_ARRAY));
		$this->assertEquals(1, F::get($var, VEG_PARAM_INARRAY, array(1,2)));
		$this->assertEquals(3, F::get($var, VEG_PARAM_INARRAY, array(3,2)));
		$this->assertEquals(array(1 => 1, 333 => 333), F::get($var, VEG_PARAM_INTS));
		$this->assertEquals(false, F::get($var, VEG_PARAM_DATE));
		$this->assertEquals(false, F::get($var, VEG_PARAM_DATETIME));
		$this->assertEquals(false, F::get($var, VEG_PARAM_EMAIL));
		$this->assertEquals('1333', F::get($var, VEG_PARAM_PHONE));
		$this->assertEquals('adgadfgsdfg,1,333,&lt;html&gt;\'dfgsdgsdfgsdfgsdfg&lt;/html&gt;', F::get($var, VEG_PARAM_EDIT));
	}

	public function testGetArray()
	{
		$var = 'test';
		$val = array(1, 'dgdg', 2);
		F::setGetArray(array($var => $val));
		$this->assertEquals($val, F::get($var));

		try {
			$this->assertEquals('no error!', F::get($var, VEG_PARAM_INT));
		} catch (\Exception $e) {
			$this->assertEquals('F::get array param test taken as int', $e->getMessage());
		}

		try {
			$this->assertEquals('no error!', F::get($var, VEG_PARAM_INT, 33));
		} catch (\Exception $e) {
			$this->assertEquals('F::get array param test taken as int', $e->getMessage());
		}

		try {
			$this->assertEquals('no error!', F::get($var, VEG_PARAM_INT, 'i9oiojip'));
		} catch (\Exception $e) {
			$this->assertEquals('F::get array param test taken as int', $e->getMessage());
		}

		$this->assertEquals($val, F::get($var, VEG_PARAM_ARRAY));

		try {
			$this->assertEquals('no error!', F::get($var, VEG_PARAM_INARRAY, array(1,2)));
		} catch (\Exception $e) {
			$this->assertEquals('F::get array param test taken as inarray', $e->getMessage());
		}

		$this->assertEquals(array(1 => 1, 2 => 2), F::get($var, VEG_PARAM_INTS));

		try {
			$this->assertEquals('no error!', F::get($var, VEG_PARAM_DATE));
		} catch (\Exception $e) {
			$this->assertEquals('F::get array param test taken as date', $e->getMessage());
		}
		try {
			$this->assertEquals('no error!', F::get($var, VEG_PARAM_DATETIME));
		} catch (\Exception $e) {
			$this->assertEquals('F::get array param test taken as datetime', $e->getMessage());
		}
		try {
			$this->assertEquals('no error!', F::get($var, VEG_PARAM_EMAIL));
		} catch (\Exception $e) {
			$this->assertEquals('F::get array param test taken as email', $e->getMessage());
		}
		try {
			$this->assertEquals('no error!', F::get($var, VEG_PARAM_PHONE));
		} catch (\Exception $e) {
			$this->assertEquals('F::get array param test taken as phone', $e->getMessage());
		}

		try {
			$this->assertEquals('no error!', F::get($var, VEG_PARAM_EDIT));
		} catch (\Exception $e) {
			$this->assertEquals('F::get array param test taken as edit', $e->getMessage());
		}
	}

	public function testGetNone()
	{
		$var = 'test';
		F::setGetArray(array());
		$this->assertEquals(false, F::get($var));

		$this->assertEquals(false, F::get($var, VEG_PARAM_INT));
		$this->assertEquals(33, F::get($var, VEG_PARAM_INT, 33));
		$this->assertEquals(false, F::get($var, VEG_PARAM_INT, 'jk;jkl;'));

		$this->assertEquals(array(), F::get($var, VEG_PARAM_ARRAY));
		$this->assertEquals(1, F::get($var, VEG_PARAM_INARRAY, array(1,2)));
		$this->assertEquals(3, F::get($var, VEG_PARAM_INARRAY, array(3,2)));
		$this->assertEquals(array(), F::get($var, VEG_PARAM_INTS));
		$this->assertEquals(false, F::get($var, VEG_PARAM_DATE));
		$this->assertEquals(false, F::get($var, VEG_PARAM_DATETIME));
		$this->assertEquals(false, F::get($var, VEG_PARAM_EMAIL));
		$this->assertEquals(false, F::get($var, VEG_PARAM_PHONE));
		$this->assertEquals(false, F::get($var, VEG_PARAM_EDIT));

	}

	public function testIp()
	{
		global $_SERVER, $HTTP_SERVER_VARS;
		unset($_SERVER['HTTP_X_REAL_IP']);
		unset($_SERVER['REMOTE_ADDR']);
		unset($_SERVER['HTTP_X_FORWARDED_FOR']);

		$this->assertEquals(false, F::ip());

		putenv('REMOTE_ADDR=192.133.63.229');
		$this->assertEquals('192.133.63.229', F::ip());

		$HTTP_SERVER_VARS['REMOTE_ADDR'] = '192.134.63.229 1';
		$this->assertEquals(false, F::ip());

		$_SERVER['REMOTE_ADDR'] = '10.135.63.';
		$this->assertEquals(false, F::ip());

		$_SERVER['HTTP_X_FORWARDED_FOR'] = '10.136.63.229';
		$this->assertEquals('10.136.63.229', F::ip());

		$_SERVER['HTTP_X_REAL_IP'] = '10.137.63.229 1';
		$this->assertEquals(false, F::ip());

		$_SERVER['HTTP_X_REAL_IP'] = '192.138.63.';
		$this->assertEquals(false, F::ip());
	}
}
