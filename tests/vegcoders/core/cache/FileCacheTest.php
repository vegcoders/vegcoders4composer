<?php
namespace vegcoders\core\cache\engines;

use vegcoders\core\cache\Cache;

class FileCacheTest extends \PHPUnit_Framework_TestCase
{

	public function testFileCache()
	{
		Cache::setDebug(false);

		$key = 'test';
		$key2 = 'te someother <bad words>\'.';
		$key3 = 'notte someother <bad words>\'.';
		$value = 'value';
		$value2 = 'value other';
		$engine = new FileCache();
		$engine->removeAll();

		$this->assertEquals(true, $engine->save($key, $value));
		$this->assertFileExists($engine->_readyName($key));
		$this->assertEquals('<?php $key = \'' . $key . '\'; $cached = \'' . $value . '\';', file_get_contents($engine->_readyName($key)));

		$this->assertEquals($value, $engine->get($key));

		$this->assertEquals(true, $engine->save($key, $value2));
		$this->assertEquals($value2, $engine->get($key));


		$this->assertEquals(true, $engine->save($key, $value));
		$this->assertEquals(true, $engine->save($key2, $value2));
		$this->assertEquals(true, $engine->save($key3, $value2));

		$all = $engine->getAll('te');
		$this->assertArrayHasKey($key, $all);
		$this->assertArrayHasKey($key2, $all);
		$this->assertArrayNotHasKey($key3, $all);
		$this->assertEquals($value, $all[$key]);
		$this->assertEquals($value2, $all[$key2]);

		$this->assertEquals(true, $engine->remove($key));
		$this->assertEquals(false, $engine->get($key));
		$this->assertEquals($value2, $engine->get($key2));



		$this->assertEquals(true, $engine->removeAll('te'));
		$all = $engine->getAll('te');
		$this->assertEquals(array(), $all);
		$this->assertEquals($value2, $engine->get($key3));

		$engine->removeAll();
	}
}