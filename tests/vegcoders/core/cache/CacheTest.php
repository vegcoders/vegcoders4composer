<?php
namespace vegcoders\core\cache;


class CacheTest extends \PHPUnit_Framework_TestCase
{

	public function testCache()
	{
		$key = 'test1';
		$value = 'test string1';
		$value2 = 'test string2';

		Cache::setDebug(false);

		Cache::put($key, $value, Cache::FILE_ENGINE);
		$this->assertEquals($value, Cache::get($key, Cache::FILE_ENGINE));
		$this->assertEquals(false, Cache::get($key, Cache::NONE_ENGINE));

		Cache::put($key, $value2, Cache::NONE_ENGINE);
		$this->assertEquals(false, Cache::get($key, Cache::NONE_ENGINE)); //none engine none cache
		$this->assertEquals($value, Cache::get($key, Cache::FILE_ENGINE));

		Cache::setDefaultEngine(Cache::NONE_ENGINE);
		Cache::put($key, $value2);
		$this->assertEquals($value2, Cache::get($key)); //cached
	}
}
