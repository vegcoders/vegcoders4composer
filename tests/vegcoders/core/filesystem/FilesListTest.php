<?php
namespace vegcoders\core\filesystem;

class FilesListTest extends \PHPUnit_Framework_TestCase
{
	public function testFilesList()
	{
		$files = FilesList::listTestsFromDir(__DIR__);
		$this->assertEquals(array('\FilesListTest.php' => __DIR__ . '\FilesListTest.php'), $files);
	}
}