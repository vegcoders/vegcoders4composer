<?php
namespace vegcoders\core\db;

require_once __DIR__ . '/../DBInsertTest.php';

use vegcoders\core\db\DBInitParams;

class DBMysqlInsertTest extends DBInsertTest
{
	public function setUp()
	{
		$this->engine = 'mysql';

		DB::initEngine(DB::MYSQL_ENGINE, 'mysql', new DBParams(
			array('host' => 'localhost', 'db' => 'turtus_test', 'user' => 'root', 'pass' => '1111')
		));
		DB::setDefaultEngine('mysql');
	}

	public function tearDown()
	{
		DB::removeEngine('mysql');
		parent::tearDown();
	}
}