<?php
namespace vegcoders\core\db;

use vegcoders\core\db\DBInitParams;
use vegcoders\core\db\engines\PostgreDB;

class DBInsertTest extends \PHPUnit_Framework_TestCase
{

	public $engine;
	public function setUp()
	{
		$this->engine = 'postgre';
		DB::setDefaultEngine(false);
	}
	
	public function testInsertOne()
	{
		DB::setDebug(false);

		try {
			$results = DB::insert('insert_test_no_table', array('title' => 'some_title'));
			$this->assertEquals(false, $results);
		} catch (\Exception $e) {
			$this->assertEquals(0, strpos(
					strip_tags($e->getMessage()),
					'DB connection: turtus_test1<br/>DB error:ERROR:  relation "insert_test_no_table" does not exist')
			);
		}

		DB::truncate('insert_test');
		$results = DB::insert('insert_test', ['title' => 'some_title2']);
		$this->assertEquals(1, $results);

		DB::truncate('insert_test');
		$results = DB::insert('insert_test', ['title' => 'русский']);
		$this->assertEquals(1, $results);
		$results = DB::query('SELECT * FROM insert_test WHERE title=' . DBHelper::escape('русский', true));
		$this->assertEquals('русский', $results['title']);
		$this->assertEquals('1', $results['id']);


		DB::truncate('insert_test');
		$results = DB::insertMulti('insert_test', [['title' => 'some_title3'], ['title' => 'some_title4', 'crtime' => 'NOW()']] );
		$this->assertEquals([1 => 1, 2 => 2], $results);

		DB::truncate('insert_test');
		$results = DB::insert('insert_test', ['title' => 'some_title2'], false);
		$this->assertEquals(true, $results);

		DB::truncate('insert_test');
		$results = DB::insertMulti('insert_test', [['title' => 'some_title3'], ['title' => 'some_title4', 'crtime' => 'NOW()']], false );
		$this->assertEquals(true, $results);


		//table with not primary key
		DB::truncate('insert_test_no_key');
		$results = DB::insert('insert_test_no_key', ['title' => 'some_title2']);
		$this->assertEquals(1, $results);

		DB::truncate('insert_test_no_key');
		$results = DB::insertMulti('insert_test_no_key', [['title' => 'some_title3'], ['title' => 'some_title4']] );
		$this->assertEquals([1 => 1, 2 => 2], $results);

		DB::truncate('insert_test_no_key');
		$results = DB::insert('insert_test_no_key', ['title' => 'some_title2'], false);
		$this->assertEquals(true, $results);

		DB::truncate('insert_test_no_key');
		$results = DB::insertMulti('insert_test_no_key', [['title' => 'some_title3'], ['title' => 'some_title4']], false );
		$this->assertEquals(true, $results);



		//table with field ident
		DB::truncate('insert_test_ident');
		$results = DB::insert('insert_test_ident', ['title' => 'some_title2'], 'ident');
		$this->assertEquals(1, $results);

		DB::truncate('insert_test_ident');
		$results = DB::insertMulti('insert_test_ident', [['title' => 'some_title3'], ['title' => 'some_title4']], 'ident' );
		$this->assertEquals([1 => 1, 2 => 2], $results);

		DB::truncate('insert_test_ident');
		$results = DB::insert('insert_test_ident', ['title' => 'some_title2'], false);
		$this->assertEquals(true, $results);

		DB::truncate('insert_test_ident');
		$results = DB::insertMulti('insert_test_ident', [['title' => 'some_title3'], ['title' => 'some_title4']], false );
		$this->assertEquals(true, $results);


		//table with no serial field
		if ($this->engine == 'postgre') {
			DB::truncate('insert_test_no_serial');
			$results = DB::insert('insert_test_no_serial', ['title' => 'some_title2'], 'title');
			$this->assertEquals('some_title2', $results);

			DB::truncate('insert_test_no_serial');
			$results = DB::insertMulti('insert_test_no_serial', [['title' => 'some_title3'], ['title' => 'some_title4']], 'title');
			$this->assertEquals(['some_title3' => 'some_title3', 'some_title4' => 'some_title4'], $results);
		}

		DB::truncate('insert_test_no_serial');
		$results = DB::insert('insert_test_no_serial', ['title' => 'some_title2'], false);
		$this->assertEquals(true, $results);

		DB::truncate('insert_test_no_serial');
		$results = DB::insertMulti('insert_test_no_serial', [['title' => 'some_title3'], ['title' => 'some_title4']], false );
		$this->assertEquals(true, $results);

	}
}