<?php
namespace vegcoders\core\db;

use vegcoders\core\db\DBInitParams;
use vegcoders\core\db\engines\PostgreDB;

class DBUpdateTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		DB::setDefaultEngine(false);
	}
	public function testUpdateOne()
	{
		DB::setDefaultEngine();
		DB::truncate('insert_test');
		try {
			$results = DB::update('insert_test', 1, ['title' => 'some_title2']);
			$this->assertEquals(false, $results);
		} catch (\Exception $e) {

		}
		$results = DB::insert('insert_test', ['title' => 'some_title2']);
		$this->assertEquals(1, $results);

		$results = DB::update('insert_test', 1, ['title' => 'some_title22']);
		$this->assertEquals(true, $results);
		$title = DB::query('SELECT title FROM insert_test WHERE id=1', DB::RETURN_ONE);
		$this->assertEquals('some_title22', $title);

		$results = DB::update('insert_test', 'WHERE id=1', ['title' => 'some_title55']);
		$this->assertEquals(true, $results);
		$title = DB::query('SELECT title FROM insert_test WHERE id=1', DB::RETURN_ONE);
		$this->assertEquals('some_title55', $title);

		$results = DB::update('insert_test', 'WHERE id=1', 'title = \'some_title66\'');
		$this->assertEquals(true, $results);
		$title = DB::query('SELECT title FROM insert_test WHERE id=1', DB::RETURN_ONE);
		$this->assertEquals('some_title66', $title);
	}
}