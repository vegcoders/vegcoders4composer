<?php
namespace vegcoders\core\db;

use AppSiteSettings;

class DBSeveralTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		DB::setDefaultEngine(false);
	}
	public function testDB()
	{
		$connection_1 = 'test';
		$connection_2 = 'test2';
		DB::initEngine(DB::POSTGRE_ENGINE, $connection_1, new DBParams(
			array('host' => 'localhost', 'db' => 'turtus_test2', 'user' => AppSiteSettings::DB_USER, 'pass' => AppSiteSettings::DB_PASS)
		));
		DB::initEngine(DB::POSTGRE_ENGINE, $connection_2, new DBParams(
			array('host' => 'localhost', 'db' => 'turtus_test3', 'user' => AppSiteSettings::DB_USER, 'pass' => AppSiteSettings::DB_PASS)
		));

		try {
			$results = DB::query('SELECT * FROM test_only_in_first', DB::RETURN_ONE, $connection_2);
			$this->assertEquals(false, $results);
		} catch (\Exception $e) {
			$this->assertEquals(0, strpos(
					strip_tags($e->getMessage()),
					'DB connection: vegshop_test2DB error:ERROR:  relation "only_in_first_test" does not exist')
			);
		}

		$results = DB::query('SELECT id, title FROM test_only_in_first LIMIT 1', DB::RETURN_ONE, $connection_1);
		$this->assertEquals(array('id' => 1, 'title' => 'only_in_first_title'), $results);
	}
}