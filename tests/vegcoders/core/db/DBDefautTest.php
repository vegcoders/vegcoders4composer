<?php
namespace vegcoders\core\db;

use vegcoders\core\db\DBInitParams;
use vegcoders\core\db\DB;

class DBDefaultTest extends \PHPUnit_Framework_TestCase
{
	public $engine;
	public function setUp()
	{
		$this->engine = 'postgre';
		DB::setDefaultEngine(false);
	}

	public function testDefaultDB()
	{
		DB::setDebug(false);

		/** @var DB $db_object */
		$db_object = DB::getInstance();
		/** @var PostgreDB $db_engine */
		$db_engine = $db_object->getEngine();
		if ($this->engine == 'postgre') {
			$this->assertEquals('vegcoders\core\db\engines\PostgreDB', get_class($db_engine));
		} else {
			$this->assertNotEquals('vegcoders\core\db\engines\PostgreDB', get_class($db_engine));
		}

		$results = DB::query('SELECT id FROM test_columns', DB::RETURN_LINK);
		if ($this->engine == 'postgre') {
			$this->assertInternalType('resource', $results);
		}

		$results = DB::query('SELECT id FROM test_columns', DB::RETURN_LIST);
		$this->assertEquals([1 => 1, 2 => 2], $results);

		$results = DB::query('SELECT id FROM test_columns', DB::RETURN_ALIST);
		$this->assertEquals([1 => ['id' => 1], 2 => ['id' => 2]], $results);

		$results = DB::query('SELECT id FROM test_columns', DB::RETURN_ONE);
		$this->assertEquals(1, $results);

		$results = DB::query('SELECT id FROM test_columns');
		$this->assertEquals(1, $results);


		//two fields
		$results = DB::query('SELECT id, title FROM test_columns', DB::RETURN_LINK);
		if ($this->engine == 'postgre') {
			$this->assertInternalType('resource', $results);
		}

		$results = DB::query('SELECT id, title FROM test_columns', DB::RETURN_LIST);
		$this->assertEquals([1 => 'value1', 2 => 'value2'], $results);

		$results = DB::query('SELECT id, title FROM test_columns', DB::RETURN_ALIST);
		$this->assertEquals([1 => ['id' => 1, 'title' => 'value1'], 2 => ['id' => 2, 'title' => 'value2']], $results);

		$results = DB::query('SELECT id, title FROM test_columns', DB::RETURN_MLIST);
		$this->assertEquals([1 => ['value1' => 'value1'], 2 => ['value2' => 'value2']], $results);

		$results = DB::query('SELECT id, title FROM test_columns', DB::RETURN_ONE);
		$this->assertEquals(['id' => 1, 'title' => 'value1'], $results);

		$results = DB::query('SELECT id, title FROM test_columns');
		$this->assertEquals(['id' => 1, 'title' => 'value1'], $results);


		//three fields
		$results = DB::query('SELECT id, title, subtitle FROM test_columns', DB::RETURN_LINK);
		if ($this->engine == 'postgre') {
			$this->assertInternalType('resource', $results);
		}

		$results = DB::query('SELECT id, title, subtitle FROM test_columns', DB::RETURN_LIST);
		$this->assertEquals([1 => ['value1', 'subvalue1'], 2 => ['value2', 'subvalue2']], $results);

		$results = DB::query('SELECT id, title, subtitle FROM test_columns', DB::RETURN_ALIST);
		$this->assertEquals([1 => ['id' => 1, 'title' => 'value1', 'subtitle' => 'subvalue1'], 2 => ['id' => 2, 'title' => 'value2', 'subtitle' =>
			'subvalue2']], $results);

		$results = DB::query('SELECT id, title, subtitle FROM test_columns', DB::RETURN_MLIST);
		$this->assertEquals([1 => ['value1' => 'subvalue1'], 2 => ['value2' => 'subvalue2']], $results);

		$results = DB::query('SELECT id, title, subtitle FROM test_columns', DB::RETURN_MMLIST);
		$this->assertEquals([1 => ['value1' => ['subvalue1' => 'subvalue1']], 2 => ['value2' => ['subvalue2' => 'subvalue2']]], $results);

		$results = DB::query('SELECT id, title, subtitle FROM test_columns', DB::RETURN_ONE);
		$this->assertEquals(['id' => 1, 'title' => 'value1', 'subtitle' => 'subvalue1'], $results);

		$results = DB::query('SELECT id, title, subtitle FROM test_columns');
		$this->assertEquals(['id' => 1, 'title' => 'value1', 'subtitle' => 'subvalue1'], $results);


		//four  fields
		$results = DB::query('SELECT id, title, subtitle, subsubtitle FROM test_columns', DB::RETURN_LINK);
		if ($this->engine == 'postgre') {
			$this->assertInternalType('resource', $results);
		}

		$results = DB::query('SELECT id, title, subtitle, subsubtitle FROM test_columns', DB::RETURN_LIST);
		$this->assertEquals([1 => ['value1', 'subvalue1', 'subsubvalue1'], 2 => ['value2', 'subvalue2', 'subsubvalue2']], $results);

		$results = DB::query('SELECT id, title, subtitle, subsubtitle FROM test_columns', DB::RETURN_ALIST);
		$this->assertEquals([
			1 => ['id' => 1, 'title' => 'value1', 'subtitle' => 'subvalue1', 'subsubtitle' => 'subsubvalue1'],
			2 => ['id' => 2, 'title' => 'value2', 'subtitle' => 'subvalue2', 'subsubtitle' => 'subsubvalue2']
		], $results);

		$results = DB::query('SELECT id, title, subtitle, subsubtitle FROM test_columns', DB::RETURN_MLIST);
		$this->assertEquals([
			1 => [['title' => 'value1', 'subtitle' => 'subvalue1', 'subsubtitle' => 'subsubvalue1']],
			2 => [['title' => 'value2', 'subtitle' => 'subvalue2', 'subsubtitle' => 'subsubvalue2']]
		], $results);

		$results = DB::query('SELECT id, title, subtitle, subsubtitle FROM test_columns', DB::RETURN_MMLIST);
		$this->assertEquals([
			1 => ['value1' => [['subtitle' => 'subvalue1', 'subsubtitle' => 'subsubvalue1']]],
			2 => ['value2' => [['subtitle' => 'subvalue2', 'subsubtitle' => 'subsubvalue2']]]
		], $results);

		$results = DB::query('SELECT id, title, subtitle, subsubtitle FROM test_columns', DB::RETURN_ONE);
		$this->assertEquals(['id' => 1, 'title' => 'value1', 'subtitle' => 'subvalue1', 'subsubtitle' => 'subsubvalue1'], $results);

		$results = DB::query('SELECT id, title, subtitle, subsubtitle FROM test_columns');
		$this->assertEquals(['id' => 1, 'title' => 'value1', 'subtitle' => 'subvalue1', 'subsubtitle' => 'subsubvalue1'], $results);

	}
}
