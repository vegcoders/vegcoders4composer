<?php
namespace vegcoders\api;

class AuthTokenTest extends \PHPUnit_Framework_TestCase
{
	public function testToken()
	{
		$tokens = array();
		for($id = 1; $id <= 10; $id++) {
			$token = AuthToken::create($id);
			if (isset($tokens[$token])) {
				$this->fail('token doubled! ' . $token);
			}
			$tokens[$token] = $id;
			$this->assertEquals($id, AuthToken::check($token));
			$this->assertNotEquals($id, AuthToken::check($token . 'r'));
			$this->assertNotEquals($id, AuthToken::check('r' . $token));
		}
	}
}
