<?php
namespace vegcoders\db_tables;

require_once  __DIR__ . '/../../../autoload.php';

use vegcoders\core\db\DB;
use vegcoders\db_tables\logs\Error;
use AppEnginesSettings;

class ErrorTest extends \PHPUnit_Framework_TestCase
{
	public function testError()
	{
		DB::query('DELETE FROM ' . AppEnginesSettings::DB_TABLE_LOGS_ERRORS);
		$error = new Error();
		$error->code = 999;
		$error->title = 'test';
		$error->source = 'source';
		$error->description = 'test desc';
		$error->insert();
		$saved = DB::query('SELECT * FROM ' . AppEnginesSettings::DB_TABLE_LOGS_ERRORS, 'array');
		$saved = $saved[0];
		$this->assertEquals($error->code, $saved['code']);
		$this->assertEquals($error->title, $saved['title']);
		$this->assertEquals($error->source, $saved['source']);
		$this->assertEquals($error->description, $saved['description']);
	}
}
