<?php
namespace vegcoders\db_tables;

require_once  __DIR__ . '/../../../autoload.php';

use vegcoders\db_tables\content\PageOne;
use vegcoders\db_tables\content\Pages;

class PagesTest extends \PHPUnit_Framework_TestCase
{
	public function testMail()
	{
		$pages = new Pages();

		//взять все по умолчанию
		$results = $pages->loadAll();
		$this->assertArrayHasKey(0, $results);

		/** @var PageOne $value */
		$value = $results[0];
		$this->assertEquals('test title', $value->title);
		$this->assertEquals('test text', $value->text);
		$this->assertEquals(1, $value->id);

		//доп параметры
		$results = $pages->setFields('id,title')->setOrderBy('id', 'DESC')->loadAll();
		$value = $results[0];
		$this->assertEquals('title2', $value->title);
		$this->assertEquals(false, $value->text);
		$this->assertEquals(2, $value->id);

		//результат сбросился
		$results = $pages->loadAll();
		$value = $results[0];
		$this->assertEquals('test title', $value->title);
		$this->assertEquals('test text', $value->text);
		$this->assertEquals(1, $value->id);

		//условия
		$results = $pages->setFields('id, title')->setWhere('id=2')->loadAll();
		$value = $results[0];
		$this->assertEquals(1, count($results));
		$this->assertEquals('title2', $value->title);
		$this->assertEquals(false, $value->text);
		$this->assertEquals(2, $value->id);

		//нужен только один объект
		$value = $pages->setWhere('id=2')->loadOne();
		$this->assertEquals('title2', $value->title);
		$this->assertEquals('text2', $value->text);
		$this->assertEquals(2, $value->id);
	}
}
