<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once __DIR__ . '/../vendor/autoload.php';

class AppEnginesSettings extends VegEnginesSettings {

}
class AppSiteSettings extends VegSiteSettings {
	const DEBUG = true;
	const DB_HOST = 'localhost';
	const DB_USER = 'postgres';
	const DB_PASS = '111';
	const DB_NAME = 'turtus_test1';
}