<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit7432dd479c002d247800bc569600742c
{
    public static $files = array (
        '3a803e8a147d5411fe3f42e461fa5cc8' => __DIR__ . '/../..' . '/src/_constants.php',
        'e4297f342d6fc27c1cddedc17e55cdb3' => __DIR__ . '/../..' . '/src/_engines_settings.php',
        '6baf5c0de37fe8a4442d5d469fd55ee5' => __DIR__ . '/../..' . '/src/_site_settings.php',
    );

    public static $prefixLengthsPsr4 = array (
        'v' => 
        array (
            'vegcoders\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'vegcoders\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/vegcoders',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit7432dd479c002d247800bc569600742c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit7432dd479c002d247800bc569600742c::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
