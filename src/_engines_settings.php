<?php

/** @noinspection PhpIllegalPsrClassPathInspection */
class VegEnginesSettings {
	const API_KEY 						= '4g5';
	const API_KEY_AFTER					= 'dff524De';

	const CORE_CACHE_ENGINE 			= 'FileCache';
	const CORE_CACHE_FILE_PREFIX 		= '';

	const CORE_CACHE_MEMCACHED_HOST 	= '127.0.0.1';
	const CORE_CACHE_MEMCACHED_PORT 	= '11211';
	const CORE_CACHE_MEMCACHED_PREFIX 	= '';
	const CORE_CACHE_MEMCACHED_LIFETIME = 0;

	const CORE_MAIL_ENGINE = 'UsualMail';
	const CORE_MAIL_QUERY_DB_TABLE = 'queries.emails';

	const CORE_DB_ENGINE = 'PostgreDB';
	const DB_TABLE_LOGS_DEBUG = 'logs.debug';
	const DB_TABLE_LOGS_ERRORS = 'logs.errors';

	const DB_TABLE_CONTENT_BLOCKS = 'content.blocks';
	const DB_TABLE_CONTENT_PAGES = 'content.pages';
}