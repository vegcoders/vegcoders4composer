<?php
/**
 * класс-обертка для работы с сессиями (могут храниться в мемкеше и тд)
 */
namespace vegcoders\core\session;

if (!defined('ENV_TYPE')) {
	define('ENV_TYPE', 'site');
}

use vegcoders\core\funcs\FT;
use vegcoders\core\funcs\F;
use Exception;

class Session
{
	private static $was_started = false;
	private static $was_changed = false;
	private static $was_ended = false;
	private static $was_ended_from = '';


	static public function start()
	{
		if (self::$was_ended) {
			throw new Exception('Session was already ended ' . print_r(self::$was_ended_from, true));
		}
		if (self::$was_started) {
			return true;
		}
		self::$was_started = true;
		session_start();
		return true;
	}
	static public function put($var, $val = null)
	{
		self::start();
		if (is_array($var)){
			if ($val) {
				throw new Exception('do not know what to do');
			}
			foreach ($var AS $v => $value){
				$_SESSION[$v] = $value;
			}
		} else {
			$_SESSION[$var] = $val;
		}
		self::$was_changed = true;
	}
	static public function get($var)
	{
		self::start();
		if (is_array($var)){
			$result = array();
			foreach ($var AS $v => $val){
				$result[$v] = @$_SESSION[$v] ?? false;
			}
			return $result;
		} else {
			return @$_SESSION[$var] ?? false;
		}
	}
	static public function putAndEnd($var, $val = null)
	{
		self::put($var, $val);
		self::end();
	}
	static public function getAndEnd($var)
	{
		$get = self::get($var);
		self::end();
		return $get;
	}
	static public function getLike($var_template)
	{
		self::start();
		$result = array();
		foreach ($_SESSION AS $var => $val) {
			//dict_ => dict_blocks => true
			if (strpos($var_template, $var) === 0) {
				$result[$var] = $val;
			}
		}
		return $result;
	}
	static public function getKey()
	{
		self::start();
		return uniqid('', true);
	}
	static public function remove($var) {
		self::start();
		if (isset($_SESSION[$var])) {
			unset($_SESSION[$var]);
			self::$was_changed = true;
		}
		return true;
	}
	static public function removeLike($var_template)
	{
		self::start();
		foreach ($_SESSION AS $var => $val) {
			//dict_ => dict_blocks => true
			if (strpos($var_template, $var) === 0) {
				unset($_SESSION[$var]);
				self::$was_changed = true;
			}
		}
		return true;
	}

	/**
	 * finds page to return to (different forms - different return pages)
	 * @param string|bool $code
	 * @return string
	 * @throws Exception
	 */
	static public function getRP($code = false)
	{
		if (!$code) {
			$code = FT::prepUrl(F::uri());
		}
		$rp = Session::get('rp_' . $code);
		if (!$rp) {
			$rp = F::referer();
			Session::put('rp_' . $code, $rp);
		}
		return $rp;
	}

	/**
	 * redirects to page after form done
	 * @param string $default - default url
	 * @param string|bool $code - code to separate different forms
	 * @return void
	 * @throws \Exception
	 */
	static public function gotoRP($default = '/', $code = false)
	{
		if (!$code) {
			$code = FT::prepUrl(F::uri());
		}
		$rp = Session::get('rp_' . $code);
		Session::remove('rp_' . $code);
		header('Location:' . ($rp ?: $default));
		exit;
	}

	/**
	 * generates secure key for the form
	 * @param string|bool $code - code to separate different forms
	 * @return string
	 * @throws \Exception
	 */
	static public function generateSK($code = false)
	{
		if (!$code) {
			$code = FT::prepUrl(F::uri());
		}
		$sk = uniqid('', true);
		Session::put('sk_' . $code, $sk);
		return $sk;
	}

	/**
	 * checks secure key for the form
	 * @param string|bool $code - code to separate different forms
	 * @return bool
	 * @throws \Exception
	 */
	static public function checkSK($code = false)
	{
		if (!$code) {
			$code = FT::prepUrl(F::uri());
		}
		$ok = Session::get('sk_' . $code) === F::get('sk');
		Session::remove('sk_' . $code);
		return $ok;
	}

	static public function removeArrayElement($var, $key)
	{
		if (is_array($_SESSION[$var]) && isset($_SESSION[$var][$key])) {
			unset($_SESSION[$var][$key]);
		}
		return true;
	}

	static public function end()
	{
		self::$was_ended = true;
		self::$was_ended_from = debug_backtrace();
		if (self::$was_changed) {
			session_write_close();
		} else {
			session_abort();
		}
	}
}
