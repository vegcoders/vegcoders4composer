<?php
namespace vegcoders\core\filesystem;

use vegcoders\core\cache\Cache;
use vegcoders\core\funcs\FT;

class FilesList {
	/**
	 * tests files search
	 * @param string $path - Tests classes path
	 * @return array
	 */
	public static function listTestsFromDir($path) {
		return self::listFromDir($path, '|(.*)Test.php$|i', true);
	}

	/**
	 * file recursive search
	 * @param string $path
	 * @param string $regular_expression
	 * @param bool $find_classes - include class name in files list (return than class name => file name list)
	 * @param bool $use_cache
	 * @return array
	 */
	/** @noinspection MoreThanThreeArgumentsInspection */
	public static function listFromDir($path, $regular_expression = '/^.+\.php$/i', $find_classes = false, $use_cache = true)
	{
		$cache = 'list_' . md5($path . $regular_expression . $find_classes);
		if ($use_cache) {
			$result = Cache::get($cache);
			if ($result) {
				return $result;
			}
		}
		if (!is_dir($path)) {
			return array();
		}
		$directory  = new \RecursiveDirectoryIterator($path);
		$iterator   = new \RecursiveIteratorIterator($directory);
		$array      = new \RegexIterator($iterator, $regular_expression, \RecursiveRegexIterator::GET_MATCH);
		$result     = array();
		if ($find_classes) {
			foreach($array AS $row) {
				$class = str_replace(array($path, '.class.php'), '', $row[0]);
				FT::fileNameToNameSpace($class);
				/** @noinspection OffsetOperationsInspection */
				$result[$class] = $row[0];
			}
		} else {
			foreach($array AS $row) {
				/** @noinspection OffsetOperationsInspection */
				$result[] = $row[0];
			}
		}
		ksort($result);
		Cache::put($cache, $result);
		return $result;
	}
}