<?php
namespace vegcoders\core\cache;

use vegcoders\core\basic\BasicSingleLogged;
use AppEnginesSettings;

/**
 * Класс единого кеша, разные подключения через "двигатель"
 */
class Cache extends BasicSingleLogged
{
	const FILE_ENGINE = 'FileCache';
	const MEMCACHE_ENGINE = 'MemcacheCache';
	const NONE_ENGINE = 'NoneCache';


	protected static $_instance;
	protected static $_can_debug;
	protected static $_can_debug_collect;
	/**
	 * можно ли уже загруженные переменные не перегружать
	 * @var boolean
	 */
	protected static $_use_loaded = true;
	/**
	 * движок кеша по умолчанию
	 * @var string
	 */
	protected static $_cache_default_engine = AppEnginesSettings::CORE_CACHE_ENGINE;

	protected $_loaded;
	/**
	 * все кеши запущенные (один тип движка - только один кеш можно использовать)
	 * @var array
	 */
	protected $_cache_engine = array();


	public static function setUseLoaded($use)
	{
		self::$_use_loaded = $use;
	}

	public static function setDefaultEngine($cache_class)
	{
		self::$_cache_default_engine = $cache_class;
	}

	public function getEngine($cache_class = '')
	{
		if (!$cache_class) {
			$cache_class = self::$_cache_default_engine;
		}
		if (!isset($this->_cache_engine[$cache_class])) {
			self::_log('engine:' . $cache_class);
			$cache_class_full = 'vegcoders\\core\\cache\\engines\\' . $cache_class;
			$this->_cache_engine[$cache_class] = new $cache_class_full();
		}
		return $this->_cache_engine[$cache_class];
	}

	static public function put($key, $value, $engine = false)
	{
		/** @var Cache $cache */
		$cache = self::getInstance();
		if (isset($cache->_loaded[$key]) && $cache->_loaded[$key] === $value) {
			return true;
		}

		self::_log('<b>' . $key . '</b> put to loaded', 'blue');
		$cache->_loaded[$key] = $value;
		$engine = $cache->getEngine($engine);
		$ok = $engine->save($key, $value);
		if ($ok) {
			self::_log('<b>' . $key . '</b> saved to engine', 'green');
		} else {
			self::_log('<b>' . $key . '</b> save to engine error', 'red');
		}
		return $ok;
	}

	static public function get($key, $engine = false)
	{
		/** @var Cache $cache */
		$cache = self::getInstance();
		if (!$engine && self::$_use_loaded && isset($cache->_loaded[$key])) {
			return $cache->_loaded[$key];
		}
		$cached = $cache->getEngine($engine)->get($key);

		self::_log('<b>' . $key . '</b> taken from engine', 'green');
		if (!$cached) {
			self::_log('<b>' . $key . '</b> removed from loaded');
			unset($cache->_loaded[$key]);
			return false;
		}
		self::_log('<b>' . $key . '</b> get to loaded', 'blue');
		if (self::$_use_loaded) {
			$cache->_loaded[$key] = $cached;
		}
		return $cached;
	}

	static public function getAll($prefix, $engine = false)
	{
		/** @var Cache $cache */
		$cache = self::getInstance();
		if (isset($cache->_loaded[$prefix])) {
			return $cache->_loaded[$prefix];
		}
		$caches = $cache->getEngine($engine)->getAll($prefix);
		if (!$caches) {
			return false;
		}
		$cache->_loaded[$prefix] = $caches;
		foreach ($caches AS $key => $cached) {
			if ($cached) {
				$cache->_loaded[$key] = $cached;
			} else {
				self::remove($key);
			}
		}
		return true;
	}

	static public function remove($key, $engine = false)
	{
		/** @var Cache $cache */
		$cache = self::getInstance();
		if (isset($cache->_loaded[$key])) {
			unset($cache->_loaded[$key]);
		}
		$ok = $cache->getEngine($engine)->remove($key);
		if ($ok) {
			self::_log('<b>' . $key . '</b> remove from engine', 'green');
		} else {
			self::_log('<b>' . $key . '</b> remove error from engine', 'red');
		}
		return $ok;
	}

	static public function removeAll($engine = false)
	{
		/** @var Cache $cache */
		$cache = self::getInstance();
		$ok = $cache->getEngine($engine)->removeAll();
		if ($ok) {
			$cache->_loaded = array();
			self::_log('<b>all removed from engine</b>', 'green');
		} else {
			self::_log('<b>all not removed from engine</b>', 'red');
		}
		return $ok;
	}
}