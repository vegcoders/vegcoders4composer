<?php
namespace vegcoders\core\cache\engines;

class NoneCache implements EngineCacheInterface
{
	public function save($ident, $cached)
	{
		return true;
	}
	public function get($ident)
	{
		return false;
	}
	public function getAll($prefix = false)
	{
		return false;
	}
	public function remove($ident)
	{
		return false;
	}
	public function removeAll($prefix = false)
	{
		return true;
	}
}
