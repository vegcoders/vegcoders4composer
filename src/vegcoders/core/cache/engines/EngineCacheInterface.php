<?php
namespace vegcoders\core\cache\engines;

interface EngineCacheInterface {
	public function save($ident, $cached);
	public function get($ident);
	public function getAll($prefix = false);
	public function remove($ident);
	public function removeAll($prefix = false);
}