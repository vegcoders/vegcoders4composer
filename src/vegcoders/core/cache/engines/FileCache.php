<?php
namespace vegcoders\core\cache\engines;
use vegcoders\core\filesystem\FilesList;
use AppEnginesSettings;
use AppSiteSettings;

class FileCache implements EngineCacheInterface
{
	public function save($ident, $cached)
	{
		return file_put_contents($this->_readyName($ident), '<?php $key = ' . var_export($ident, true) . '; $cached = ' . var_export($cached, true) .
			';');
	}
	public function get($ident)
	{
		/** @noinspection PhpIncludeInspection */
		@include $this->_readyName($ident);
		/** @noinspection UnSafeIsSetOverArrayInspection */
		/** @noinspection PhpUndefinedVariableInspection */
		return $cached ?? false;
	}
	public function getAll($prefix = false)
	{
		$files = FilesList::listFromDir(AppSiteSettings::TMP_PATH, '|' . AppEnginesSettings::CORE_CACHE_FILE_PREFIX . '(.*).php$|i', true, false);
		$result = array();
		if ($files) {
			foreach ($files AS $file) {
				/** @noinspection PhpIncludeInspection */
				include $file;
				/** @noinspection DisconnectedForeachInstructionInspection */
				/** @noinspection PhpUndefinedVariableInspection */
				if ($prefix && !(strpos($key, $prefix) === 0)) {
					continue;
				}
				/** @noinspection DisconnectedForeachInstructionInspection */
				/** @noinspection PhpUndefinedVariableInspection */
				/** @noinspection PhpUndefinedVariableInspection */
				$result[$key] = $cached;
			}
		}
		return $result;
	}
	public function remove($ident)
	{
		@unlink($this->_readyName($ident));
		return true;
	}
	public function removeAll($prefix = false)
	{
		$files = FilesList::listFromDir(AppSiteSettings::TMP_PATH, '|' . AppEnginesSettings::CORE_CACHE_FILE_PREFIX . '(.*).php$|i', true, false);
		if ($files) {
			foreach ($files AS $file) {
				if ($prefix) {
					/** @noinspection PhpIncludeInspection */
					include $file;
					/** @noinspection PhpUndefinedVariableInspection */
					if (!(strpos($key, $prefix) === 0)) {
						continue;
					}
				}
				@unlink($file);
			}
		}
		return true;
	}
	public function _readyName($ident)
	{
		// sha1 used to prevent illegal characters from being used in path
		return AppSiteSettings::TMP_PATH . AppEnginesSettings::CORE_CACHE_FILE_PREFIX . sha1($ident) . '.php';
	}
}
