<?php
namespace vegcoders\core\cache\engines;
use vegcoders\core\funcs\FT;
use Exception;
use AppEnginesSettings;
class MemcacheCache implements EngineCacheInterface
{

	private $mCache;

	public function __construct(){
		$this->mCache = new \Memcache();
		$this->mCache->addserver(AppEnginesSettings::CORE_CACHE_MEMCACHED_HOST, AppEnginesSettings::CORE_CACHE_MEMCACHED_PORT);
	}
	public function save($ident, $cached)
	{
		if (is_array($cached) && count($cached) > 100000) {
			throw new Exception('too big array to store');
		}
		return $this->mCache->set($this->_readyName($ident), $cached, MEMCACHE_COMPRESSED, AppEnginesSettings::CORE_CACHE_MEMCACHED_LIFETIME + time());
	}
	public function get($ident)
	{
		return $this->mCache->get($this->_readyName($ident));
	}
	public function getAll($prefix = false)
	{
		throw new Exception ('todo!');
	}
	public function remove($ident)
	{
		return $this->mCache->delete($this->_readyName($ident));
	}
	public function removeAll($prefix = false)
	{
		return $this->mCache->flush();
	}
	public function _readyName($ident)
	{
		return AppEnginesSettings::CORE_CACHE_MEMCACHED_PREFIX . md5(FT::prepClr($ident));
	}
}
