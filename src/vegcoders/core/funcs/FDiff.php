<?php
namespace vegcoders\core\funcs;
/**
 * класс для сравнения текстов
 */

class FDiff
{
	private static $_important_tags_for_diff = array('<a ', '<img', '<meta', '<link', '<script', '<form', '<table');

	/**
	 * Compare to texts on difference between strings
	 * @param   string $text_old Old text
	 * @param   string $text_new New text
	 * @param   string $string_separator Custom string serparator, "\n" by default
	 * @return  string                      Result text with html highlighting of old/new strings, null - nothing changed
	 */
	static public function diff($text_old, $text_new, $string_separator = "\n")
	{
		$output = null;
		$strings_old = explode($string_separator, $text_old);
		$strings_new = explode($string_separator, $text_new);
		$matches = array();
		foreach ($strings_new as $line_new => $string_new) {

			// If string doesn't have any important tag just strip them all for comparising
			if (!self::strposa($string_new, self::$_important_tags_for_diff)) {
				$string_new = strip_tags($string_new);
			}

			$line_old = false;
			foreach ($strings_old as $i => $string_old) {

				// If string doesn't have any important tag just strip them all for comparising
				if (!self::strposa($string_old, self::$_important_tags_for_diff)) {
					$string_old = strip_tags($string_old);
				}

				// If strings equal and it's not in the matches array yet and both strings come after last ones in matches:
				if (
					!in_array($i, $matches, false)
					&& trim($string_old) === trim($string_new)
					&& (!$matches || ($line_new > max(array_keys($matches)) && $i > max($matches)))
				) {
					// First match found. No need to cycling.
					$line_old = $i;
					break;
				}
			}
			if ($line_old !== false) {
				$matches[$line_new] = $line_old;
			}
		}

		// All old strings equal to new ones:
		$cn = count($strings_new);
		if (count($matches) === $cn && $cn === count($strings_old)) {
			return null;
		}
		$last_line_old = -1;
		$last_line_new = -1;
		foreach ($matches as $line_new => $line_old) {
			for ($i = ++$last_line_old; $i < $line_old; $i++) {
				if ($strings_old[$i]) {
					$output .= '<span style="background-color:#f99;text-decoration:line-through;">' . htmlspecialchars($strings_old[$i]) . '</span><br />';
				}
			}
			for ($i = ++$last_line_new; $i < $line_new; $i++) {
				if ($strings_new[$i]) {
					$output .= '<span style="background-color:#9f9;">' . htmlspecialchars($strings_new[$i]) . '</span><br />';
				}
			}
			$output .= htmlspecialchars($strings_new[$line_new]) . '<br />';
			$last_line_new = $line_new;
			$last_line_old = $line_old;
		}
		$ic = max(array_keys($strings_old));
		for ($i = ++$last_line_old; $i <= $ic; $i++) {
			if ($strings_old[$i]) {
				$output .= '<span style="background-color:#f99;text-decoration:line-through;">' . htmlspecialchars($strings_old[$i]) . '</span><br />';
			}
		}
		$ic = max(array_keys($strings_new));
		for ($i = ++$last_line_new; $i <= $ic; $i++) {
			if ($strings_new[$i]) {
				$output .= '<span style="background-color:#9f9;">' . htmlspecialchars($strings_new[$i]) . '</span><br />';
			}
		}
		return $output;
	}

	/**
	 * strpos analog with using array of needles instead of single string needle
	 * @param   string $haystack Where to look for
	 * @param   array $needles What to look for
	 * @param   int $offset Offset
	 * @return  bool                True - found, False - not found
	 */
	static private function strposa($haystack, $needles, $offset = 0)
	{
		/** @noinspection ArrayCastingEquivalentInspection */
		if (!is_array($needles)) {
			$needles = array($needles);
		}
		foreach ($needles as $needle) {
			if (stripos($haystack, trim($needle), $offset) !== false) {
				return true;
			}
		}
		return false;
	}
}