<?php
namespace vegcoders\core\funcs;
/**
 * класс для функций чистки
 */
use Exception;

class FCleaner
{
	public static function stripOfficeTags($string)
	{
		$string = preg_replace('/<!--.{1,10}mso.*?-->/umsi', NULL, $string);
		/** @noinspection NotOptimalRegularExpressionsInspection */
		$string = preg_replace('/\\s+class=\"Mso[^\"]*?\"\\s+style=\"[^\"]*?\"/umsi', NULL, $string);
		/** @noinspection NotOptimalRegularExpressionsInspection */
		$string = preg_replace('/\\s+style=\"[^\"]*?mso[^\"]*?\"/umsi', NULL, $string);
		/** @noinspection NotOptimalRegularExpressionsInspection */
		$string = preg_replace('/\\s+lang=\"[^\"]*?\"/umsi', NULL, $string);
		/** @noinspection NotOptimalRegularExpressionsInspection */
		$string = preg_replace('/<p\\s*>\\s*<\\/p\\s*>|<span\\s*>\\s*<\\/span\\s*>/umsi', NULL, $string);
		return $string;
	}
	static public function checkScripts($str_or_array)
	{
		if (is_array($str_or_array)) {
			$str_or_array = implode('', $str_or_array);
		}
		$str_or_array = trim($str_or_array);
		if (!$str_or_array) {
			return true;
		}
		$lower = mb_strtolower($str_or_array);
		if (preg_match('/<[^>]*?script[^<]*?>/iu', $lower)) {
			throw new Exception('script or select found');
		}
		return true;
	}
}
