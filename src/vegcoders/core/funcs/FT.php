<?php
namespace vegcoders\core\funcs;
/**
 * класс для текстовых функций
 */
use Exception;
use AppSiteSettings;
class FT
{
	/** @noinspection MoreThanThreeArgumentsInspection
	 * @param array $object - object from db to show
	 * @param string $field - value to show
	 * @param bool $len - cut to lenth
	 * @param bool $remove_quotes - remove escaped signs or not
	 * @param bool $close_tag - auto close tags for not breaking html
	 * @return mixed|string
	 */
	static public function prepField($object, $field = 'title', $len = false, $remove_quotes = false, $close_tag = false)
	{

		if (!is_array($object)) {
			$html = $object;
		} else {
			if (!isset($object[$field])) {
				return $field;
			}
			$html = $object[$field];
		}
		$html = stripslashes($html);
		if ($remove_quotes) {
			$html = str_replace(array("\\", "'", '"'), '', $html);
		}
		if ($len) {
			$html = self::substr($html, $len);
		}
		if ($close_tag) {
			$html = FT::closeTags($html);
		}
		//защита от супердлинных строк
		if (strpos($html, '<') === false && strpos($html, ' ') === false && strlen($html) > 50) {
			if (strpos($html, '/') === false) {
				$html = implode(' ', preg_split('/\\G(.{30})/su', $html, null, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE));
			} else {
				$html = implode('/ ', explode('/', $html));
			}
		}
		return $html;
	}
	static public function substr($html, $len, $end = '...')
	{
		if (mb_strlen($html, 'utf-8') < $len) {
			return $html;
		}
		return mb_substr($html, 0, $len, 'utf-8') . $end;
	}
	static public function prepDesc($text) {
		$lower = mb_strtolower($text);
		if (strpos($lower, '<p>') === false && strpos($lower, '<ul>') === false && strpos($lower, '<ol>') === false) {
			return nl2br($text);
		} else {
			return $text;
		}
	}
	
	static public function prepDate($var = false, $date_format = AppSiteSettings::DATE_FORMAT)
	{
		$time = FV::isInt($var) ? $var : strtotime($var);
		if (!$time) {
			$time = time();
		}
		return date($date_format, $time);
	}

	static public function prepDateToday($var = false)
	{
		$time = FV::isInt($var) ? $var : strtotime($var);
		if (!$time) {
			$time = time();
		}
		$date  = 'H:i';
		$today = date($date, $time);
		if (date('Y-m-d') !== date('Y-m-d', $time)) {
			$date = 'Y, F d';
			if (date('Y') === date('Y', $time)) {
				$date = 'F d';
			}
			$today = '<span>' . date($date, $time) . '</span> ' . $today;
		}
		return $today;
	}
	static public function prepTime($seconds)
	{
		$part = array();
		$part[0] = $seconds;
		//minutes
		$part[1] = floor($part[0] / 60);
		$part[0] -= $part[1] * 60;
		//hours
		$part[2] = floor($part[1] / 60);
		$part[1] -= $part[2] * 60;
		//prep
		for($i = 0; $i<3; $i++) {
			if (!$part[$i]) {
				$part[$i] = '00';
			} elseif ($part[$i] < 10) {
				$part[$i] = '0' . $part[$i];
			}
		}
		return $part[2] . ':' . $part[1] . ':' . $part[0];
	}
	
	static public function prepJs($object, $field = false, $add_q = false) {
		if (is_array($object)) {
			$html = self::prepField($object, $field, false, true);
		} else {
			$html = str_replace(array('"', "'"), '', strip_tags($object));
		}
		if ($add_q) {
			$html = '\'' . $html . '\'';
		}
		return $html;
	}
	/**
	 * сделать ссылку полной и валидной (перевод языков и тд)
	 *
	 * @param 	string	$url относительная ссылка (без адреса сайта!)
	 * @return 	string
	 * @example FT::prepUrl('projects') => http://example.com/eng/projects
	 * @example FT::prepUrl('projects', true) => http://example.com/eng/projects.html
	 * @example FT::prepUrl('projects?param1=1&param2=2', true) => http://example.com/eng/projects-q--param1-z--1-a--param2-z--2.html"
	 * @throws Exception
	 */
	static public function prepUrl($url)
	{
		$url = FT::fileNameToUrl($url);
		if (strpos($url, '#') === 0) {
			return $url;
		}
		if (strpos($url, AppSiteSettings::FRONT_SITE_URL) === 0) {
			throw new Exception($url . ' cant be doubled');
		}
		
		if (strpos($url, 'http:') === 0 || strpos($url, 'https:') === 0) {
			return $url;
		}
		if ($url === '/' || !$url) {
			return AppSiteSettings::FRONT_SITE_URL;
		}
		$url = FT::removeFirstSlash($url);
		return AppSiteSettings::FRONT_SITE_URL. $url;
	}

	static public function removeFirstSlash($url) {
		if ($url[0] === '/') {
			$url = substr($url, 1);
		}
		return $url;
	}

	static public function addLastSlash($url) {
		$lst = strlen($url) - 1;
		if ($url[$lst] !== '/') {
			$url .= '/';
		}
		return $url;
	}
	static public function removeLastSlash($url) {
		$lst = strlen($url) - 1;
		if ($url[$lst] === '/') {
			$url = substr($url, -1);
		}
		return $url;
	}
	
	/**
	 * подготовка текста к выводу в любой форме
	 *
	 * @param	string	 $text
	 * @return 	string
	 */
	static public function prepEdit($text)
	{
		return htmlspecialchars(stripslashes($text));
	}
	
	/**
	 * подготовка текста к чистому сохранению
	 *
	 * @param	string	 $text
	 * @return 	string
	 */
	static public function prepClr($text)
	{ 
		return str_replace(array('"', "'"), '', strip_tags($text));
	}
	/**
	 * закрытие тегов открытых (чтобы избежать невалидных страниц)
	 *
	 * @param	string	 	$html
	 * @return 	string html
	 */
	static public function closeTags($html)
	{
		preg_match_all('#<([a-z]+)( .*)?(?!/)>#iU', $html, $result);
		$openedtags = $closedtags = array();
		foreach ($result[1] AS $tag) {
			if (!isset($openedtags[$tag])) {
				$openedtags[$tag] = 0;
			}
			$openedtags[$tag]++;	
		}	
		preg_match_all('#</([a-z]+)>#iU', $html, $result);
		foreach ($result[1] AS $tag) {
			if (!isset($closedtags[$tag])) {
				$closedtags[$tag] = 0;
			}
			$closedtags[$tag]++;	
		}
		foreach ($openedtags AS $tag => $count) {
			if (!isset($closedtags[$tag])) {
				$html .= str_repeat('</' . $tag . '>', $count);
			} elseif ($closedtags[$tag] < $count) {
				$count -= $closedtags[$tag];
				$html .= str_repeat('</' . $tag . '>', $count);
			}
		}
		return $html;
	}
	/**
	 * перевод строки в ВерблюдоПодобныйСтиль
	 *
	 * @param	string	 	$value		- значение
	 * @param 	boolean		$lcfirst	- первая буква с нижней строки (по умолчанию с большой)
	 * @return 	string
	 */
	static public function camelize($value, $lcfirst = false)
    {
       return preg_replace_callback('/(^|_|-|\s)([a-z0-9])/',
			function ($matches) use (&$lcfirst) {
				if ($lcfirst) { //для параметра первой буквы
					$lcfirst = false;
					return $matches[2];
				}
				return strtoupper($matches[2]);
			},
			$value);
    }
	/**
	 * перевод строки обратный из_верболюдо_подобного_в_нижнее_подчеркивание
	 *
	 * @param	string	 	$value		- значение
	 * @param 	boolean		$separator	- разделитель между словами (по умолчанию без разделителя - тогда массив возвращает, обычный вызов -  _)
	 * @return 	string
	 */
	static public function decamelize($value, $separator = false)
	{
		$lower = mb_strtolower($value, 'utf-8');
		if ($lower === $value) {
			//к нам пристали уже все в нижнем - значит верблюдов не было
			return $separator ? $value : explode('_', $value);
		}
		if ($separator){
            //$value = preg_replace("/([A-Z]?([a-z0-9]+))/e", "'" . $separator . "' . '\\1'", $value);
			$value = preg_replace_callback('/([A-Z]?([a-z0-9]+))/',
                create_function ('$matches', 'return \'' . $separator . '\' . $matches[1];'), $value);
			return strtolower(substr($value, 1));

		} else {
			$matches = array();
			/** @noinspection NotOptimalRegularExpressionsInspection */
			preg_match_all('/([A-Z]?([a-z0-9]+))/e', $value, $matches);
			$tmps = array();
			foreach ($matches[0] AS $match) {
				$tmps[] = strtolower($match);
			}
			return $tmps;
		}
	}
	/**
	 * построение "дружественной" ссылки из заголовка
	 *
	 * @param	string	 	$title		- значение
	 * @return 	string
	 */
	static public function prepFriendly($title)
	{		
		$title = trim(rtrim($title));
		$tr = array(
				'А'=>'a','Б'=>'b','В'=>'v','Г'=>'g',
				'Д'=>'d','Е'=>'e','Ё'=>'e','Ж'=>'j','З'=>'z','И'=>'i',
				'Й'=>'y','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n',
				'О'=>'o','П'=>'p','Р'=>'r','С'=>'s','Т'=>'t',
				'У'=>'u','Ф'=>'f','Х'=>'h','Ц'=>'ts','Ч'=>'ch',
				'Ш'=>'sh','Щ'=>'sch','Ъ'=>'','Ы'=>'y','Ь'=>'',
				'Э'=>'e','Ю'=>'yu','Я'=>'ya','а'=>'a','б'=>'b',
				'в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j',
				'з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l',
				'м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r',
				'с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h',
				'ц'=>'ts','ч'=>'ch','ш'=>'sh','щ'=>'sch','ъ'=>'y',
				'ы'=>'y','ь'=>'','э'=>'e','ю'=>'yu','я'=>'ya',
				'і'=>'i', 'І'=>'I', 'ї'=>'i', 'Ї'=>'I',
				'.'=> '',  '~'=> '', '/' => '-', ',' => '', '!' => '', '?' => '',
				'\"' => '', '\'' => '', '+' => '', ':' => '', ';' => '', '_' => '-',
				'----' => '-', '---' => '-', '--' => '-',
				'    ' => '-', '   ' => '-', '  '=> '-', ' ' => '-'
		);
		$title = strtolower(strtr($title, $tr));
		$title = preg_replace('/[\s\.]+/', '_', $title);
		$title = preg_replace('/[^a-z\d\_\-\s]/i', '', $title);
		$title = preg_replace('/_*\-+_*/', '-', $title);
		return $title;
	}
		

	public static function nameSpaceToFileName($class)
	{
		if ('\\' === DIRECTORY_SEPARATOR) {
			return $class;
		}
		return str_replace('\\', DIRECTORY_SEPARATOR , $class);
	}
	public static function fileNameToNameSpace($class)
	{
		if ('\\' === DIRECTORY_SEPARATOR) {
			return $class;
		}
		return str_replace(DIRECTORY_SEPARATOR, '\\', $class);
	}
	public static function fileNameToUrl($class)
	{
		if ('/' === DIRECTORY_SEPARATOR) {
			return $class;
		}
		return str_replace(DIRECTORY_SEPARATOR, '/' , $class);
	}
}