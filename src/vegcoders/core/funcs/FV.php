<?php
namespace vegcoders\core\funcs;
/**
 * класс валидатор параметров
 */
use Exception;
use vegcoders\core\debug\Error;
class FV
{
	/**
	 * @param  string | array $ids - разделенная запятыми строка или массив
	 * @return array массив номеров с выбросом пустых
	 */
	static public function stringToArray($ids)
	{
		if (!is_array($ids)) {
			$ids = explode(',', $ids);
		}
		$new_ids = array();
		foreach($ids AS $id) {
			$id = (int) $id;
			if (!$id) {
				continue;
			}
			$new_ids[$id] = $id;
		}
		return $new_ids;
	}

	/**
	 * @param  int | array $ids - переменная или массив
	 * @return string sql
	 */
	static public function intsToSql($ids)
	{
		$ids = self::stringToArray($ids);
		if (!$ids) {
			return false;
		}
		if (count($ids) === 1) {
			return ' = ' . implode('', $ids);
		} else {
			return ' IN (' . implode(',', $ids) . ')';
		}
	}

	/**
	 * проверяет массив параметров по заданным ключам (с типами переменных)
	 * @param    array $params - массив параметров (изменяется при валидации дат и времени!)
	 * @param    array $keys - ключи массива [имя => тип переменной]
	 * @throws Exception
	 */
	static public function paramsCheck(&$params, $keys) {
		if (!is_array($params)) {
			throw new Exception('errors.invalid_params', VEG_ERROR_SYSTEM_PARAMETER_INVALID);
		}
		foreach ($params AS $param => &$value) {
			if (!isset($keys[$param])) {
				throw new Exception('errors.invalid_param:' . $param, VEG_ERROR_SYSTEM_PARAMETER_INVALID);
			}
			$key_type = $keys[$param];
			if ($key_type === VEG_PARAM_INT) {
				if (!self::isInt($value)) {
					throw new Exception('errors.invalid_param_' . $param . ':' . Error::s($value), VEG_ERROR_SYSTEM_PARAMETER_INVALID);
				}
			} elseif ($key_type === VEG_PARAM_TIME) {
				if (!self::validTime($value)) {
					throw new Exception('errors.invalid_param_' . $param . ':' . Error::s($value), VEG_ERROR_SYSTEM_PARAMETER_INVALID);
				} else {
					$value	= FV::validTime($value);
				}
			} elseif ($key_type === VEG_PARAM_DATETIME) {
				if (!self::validDateTime($value)) {
					throw new Exception('errors.invalid_param_' . $param . ':' . Error::s($value), VEG_ERROR_SYSTEM_PARAMETER_INVALID);
				} else {
					$value = FV::validDateTime($value);
				}
			}
		}
	}
	/**
	 * проверяет, является ли строка числом или строкой (не массив не объект)
	 * @param 	string	$value			- собственно проверяемая строчка
	 * @return	string					- номер без лишних символов, если валидный, false иначе
	 */
	static public function isIntOrString($value)
	{
		return FV::isInt($value) || is_string($value);
	}
	/**
	 * проверяет, является ли строка числом (расширено на "строку" которая на самом деле число)
	 * @param 	string	$value			- собственно проверяемая строчка
	 * @return	boolean					- является числом, false иначе
	 */
	static public function isInt($value)
	{
		/** @noinspection CallableParameterUseCaseInTypeContextInspection */
		return is_int($value) || ctype_digit($value);
	}
	/**
	 * возвращает строку как число
	 * @param 	string	$value			- собственно проверяемая строчка
	 * @return	int
	 */
	static public function validInt($value)
	{
		return (int) $value;
	}
	/**
	 * проверяет, является ли строка временем (с зоной или без) (2014-06-16 13:58:56.616+07)
	 * @param 	string	$string			- собственно проверяемая строчка
	 * @return	string|false			- строка в правильном написании или false
	 * @todo написать!
	 */
	static public function validDateTime($string)
	{
		return $string;
	
	}
	/**
	 * проверяет, является ли строка временем (с зоной или без) (11:11:00+07)
	 * @param 	string	$string			- собственно проверяемая строчка
	 * @return	string|false			- строка в правильном написании или false
	 * @todo написать!
	 */
	static public function validTime($string)
	{
		return $string;
	}	
	/**
	 * проверяет, является ли строка датой (YYYY-MM-DD)
	 * @param 	string	$string			- собственно проверяемая строчка
	 * @return	string|false			- строка в правильном написании или false
	 */
	static public function validDate($string)
	{
		$matches = array();
		preg_match_all('#^(\d{4}-\d{2}-\d{2})([\)\.\-\s]*)?(\d{2}:\d{2})?(:\d{2})?$#', $string, $matches);
		if (!$matches[0]) {
			return false;
		}
		return $matches[1][0];
	}	
	/**
	 * проверяет, является ли строка ссылкой на страницу (на каком-то определенном домене)
	 * @param 	string	$string			- собственно проверяемая строчка
	 * @param 	string	$domain			- домен (или список) vk.com|vkontakte.ru|vkontakte.com
	 * @return	string|false			- строка в полном написании или false если совсем не тот домен
	 */
	static public function validUrl($string, $domain = '')
	{
		if ($domain) {
			return preg_match('#^https?://(www\.)?(' . $domain . ')/.+$#', $string) ? $string : false;
		}
		$tmps = parse_url($string);
		if (!$tmps || !@$tmps['host']) {
			return false;
		}
		$host = $tmps['host'];
		if (strpos($host, 'wwww.') === 0) {
			$host = trim(substr($host, 4));
		}
		$tmps = explode('.', $host);
		if (count($tmps) <= 1) {
			return false;
		}
		$domain_area = array_pop($tmps);
		if (strlen($domain_area) <= 1) {
			return false;
		}
		return $string;
	}
	/**
	 * проверяет, является ли строка номером телефона
	 * @param 	string	$string			- собственно проверяемая строчка
	 * @return	string|false			- номер без лишних символов, если валидный номер, false иначе
	 */
	static public function validPhone($string)
	{
		$string = trim($string);
		/** @noinspection NotOptimalRegularExpressionsInspection */
		$string = preg_replace('/[^\d]/', '', $string);
		if (!$string) {
			return false;
		}
		return $string;
	}
	/**
	 * проверяет, является ли строка валидным адресом электрической почты
	 * @param 	string	$string			- строка для проверки
	 * @return 	string|false			- возвращает исходную строку в нижнем регистре либо false в случае несоответствия
	 */
	static public function validEmail($string)
	{
		return filter_var(trim($string), FILTER_VALIDATE_EMAIL);
	}
}