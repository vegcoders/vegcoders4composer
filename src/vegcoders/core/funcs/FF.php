<?php
namespace vegcoders\core\funcs;

/**
 * класс для файловых функций
 */
use Exception;
use AppSiteSettings;
class FF
{
	static public function makeDirs($dirs = 'a/b/c', $root_path = '', $separator = DS)
	{
		if ($root_path) {
			$dirs = $root_path . $dirs;
		}
		if (is_file($dirs . DS)) {
			return true;
		}
		if (strpos($dirs, AppSiteSettings::ROOT_PATH) === 0) {
			$full_path = AppSiteSettings::ROOT_PATH;
			$dirs = substr($dirs, strlen(AppSiteSettings::ROOT_PATH));
		} else {
			$full_path = '';
		}
		$tmps = explode($separator, $dirs);
		foreach ($tmps AS $tmp) {
			if (!$tmp) {
				continue;
			}
			$full_path .= $tmp;
			if (!is_file($full_path . DS) && !@mkdir($full_path, 0777) && !is_dir($full_path)) {
				throw new Exception($full_path . ' cant be created', VEG_ERROR_SYSTEM_GENERAL);
			}
			$full_path .= DS;
		}
		return true;
	}

	static public function uploadFile($tmp, $path)
	{
		$dir_name = dirname(AppSiteSettings::ROOT_PATH . $path);
		FF::makeDirs($dir_name);
		return move_uploaded_file($tmp, AppSiteSettings::ROOT_PATH . $path);
	}

	/**
	 * @param $path
	 * @param bool $and_kill
	 * @return bool
	 */
	static public function cleanDir($path, $and_kill = false)
	{
		if (!is_file($path)) {
			return false;
		}
		$dir = dir($path);
		if (!$dir) {
			return false;
		}
		while (false !== ($entry = $dir->read())) {
			if ($entry === '..' || $entry === '.') {
				continue;
			}
			if (is_dir($path . DS . $entry)) {
				self::cleanDir($path . DS . $entry, true);
			} else {
				@unlink($path . DS . $entry);
			}
		}
		$dir->close();
		if ($and_kill) {
			@rmdir($path);
		}
		return true;
	}

	static public function getFileType($file)
	{
		$tmp = explode('.', $file);
		return strtolower(array_pop($tmp));
	}

	static public function getFileDate($path)
	{
		return filemtime($path);
	}

	static public function getDirFiles($path, &$files)
	{
		$dir = dir($path);
		while (false !== ($entry = $dir->read())) {
			if ($entry === '..' || $entry === '.') {
				continue;
			}
			if (is_dir($path . DS . $entry)) {
				self::getDirFiles($path . DS . $entry, $files);
			} else {
				$files[$path . DS . $entry] = $entry;
			}
		}
		$dir->close();
	}

	static public function makeFileForUrl($content, $url)
	{
		if (!strpos($url, '.html')) {
			$dir_name = $url;
			$filename = 'index.html';
		} else {
			$path = explode('/', $url);
			$filename = array_pop($path);
			if ($path[0] === '') {
				array_shift($path);
			}
			$dir_name = implode('/', $path);
		}
		$dir_name = trim($dir_name, DS);
		if (!is_dir(AppSiteSettings::PUBLIC_PATH . $dir_name)) {
			try {
				self::makeDirs(AppSiteSettings::PUBLIC_PATH . $dir_name, false, '/');
				file_put_contents(AppSiteSettings::PUBLIC_PATH . $dir_name . DS . 'index.php', '<?php require_once \'' . AppSiteSettings::PUBLIC_PATH . 'index.php\';');
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}
		if ($dir_name) {
			$dir_name .= DS;
		}
		if (!file_put_contents(AppSiteSettings::PUBLIC_PATH . $dir_name . $filename, $content)) {
			throw new Exception('File not created: ' . AppSiteSettings::PUBLIC_PATH . $dir_name . $filename);
		}
		@chmod(AppSiteSettings::ROOT_PATH . $dir_name . $filename, 0777);
		return true;
	}
}
