<?php

namespace vegcoders\core\funcs;

class FJson
{
	public static function encode($array, $remove_keys = false)
	{
		if ($remove_keys && is_array($array)) {
			$array = self::_recEncode($array);
		}
		if (defined('JSON_PRETTY_PRINT') && defined('JSON_UNESCAPED_UNICODE')) {
			return json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE); // Needs PHP >= 5.4
		} else {
			return json_encode($array);
		}
	}

	public static function _recEncode($old_rows) {
		if (!is_array($old_rows)) {
			return $old_rows;
		}
		$result = array();
		foreach ($old_rows AS $old_id => $old_row) {
			if (FV::isInt($old_id)) {
				$result[] = self::_recEncode($old_row);
			} else {
				$result[$old_id] = self::_recEncode($old_row);
			}
		}
		return $result;
	}

	public static function decode($content)
	{
		$content = str_replace("\r\n", '', $content); // protection from Windows EOLs
		return json_decode($content, true);
	}

}