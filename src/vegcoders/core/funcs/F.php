<?php
namespace vegcoders\core\funcs;

use Exception;
/**
 * общий класс функций
 */
class F 
{
	public static $_get_array;
	public static $_get_array_inited;
	public static $_angular = array();
	static public function getAngular()
	{
		if (self::$_angular) {
			return self::$_angular;
		}
		return json_decode(file_get_contents('php://input'), true);
	}
    static public function setAngularForTests($array)
    {
        self::$_angular = $array;
        self::$_get_array = $array;
        self::$_get_array_inited = true;
    }

	static public function setGetArray($array)
	{
		self::$_get_array = $array;
		self::$_get_array_inited = true;
	}

	/**
	 * получение значения из данных (тут же проверки на валидность по типу данных)
	 * массив откуда собственно брать значние, по умолчанию $_REQUEST - установка через setGetArray()
	 * @param string $var_name имя переменной в данных
	 * @param string $var_type тип переменной (VEG_PARAM_INT, VEG_PARAM_ARRAY, VEG_PARAM_INTS, VEG_PARAM_DATE, VEG_PARAM_DATETIME)
	 * @param bool|string|array $var_default значение по умолчанию, если такой переменной нет
	 * @return string |array
	 * @throws Exception
	 * @example $uid = F::get('uid', VEG_PARAM_INT)
	 */
	static public function get($var_name, $var_type = '', $var_default = false) {
		if (!self::$_get_array_inited) {
			self::setGetArray($_REQUEST);
		}
		if (!isset(self::$_get_array[$var_name])) {
			if ($var_type === VEG_PARAM_INARRAY) {
				return array_shift($var_default);
			}
			if ($var_type === VEG_PARAM_INT_INCLUDE_ZERO) {
				return$var_default;
			}
			$var  = $var_default;
		} else {
			$var = self::$_get_array[$var_name];
		}

		//защита от неявных преобразований
		$is_string = $is_array = false;
		if (is_array($var)) {
			$is_array = true;
			if ($var_type && !($var_type === VEG_PARAM_INTS) && !($var_type === VEG_PARAM_ARRAY)) {
				throw new Exception('F::get array param ' . $var_name . ' taken as ' . $var_type);
			}
		} else {
			$is_string = is_string($var);
		}

		if ($var_type === VEG_PARAM_ARRAY) {
			if (!$var) {
				return $var_default ?: array();
			}
			if (!$is_array) {
				$var = array($var);
			}
			$var = self::stripArray($var);
		} elseif ($var_type === VEG_PARAM_INT) {
			$var = (int) $var;
			$var_default = (int) $var_default;
		} elseif ($var_type === VEG_PARAM_INT_INCLUDE_ZERO) {
			if ((int) $var > 0 || $var === 0 || $var === '0') return (int) $var;
			return $var_default;
		} elseif ($var_type === VEG_PARAM_INARRAY) {
			if (!in_array($var, $var_default, false)) {
				return array_shift($var_default);
			}
			return $var;			
		} elseif ($var_type === VEG_PARAM_INTS) {
			$tmps = $var;
			$var  = array();
			if (!is_array($tmps)) {
				$tmps = explode(',', $tmps);
			}
			/** @noinspection ForeachSourceInspection */
			foreach ($tmps AS $tmp) {
				$tmp = (int) $tmp;
				if ($tmp) {
					/** @noinspection OffsetOperationsInspection */
					$var[$tmp] = $tmp;
				}
			}
			if (!$var) {
				return $var_default ?: array();
			}
		} elseif ($var_type === VEG_PARAM_DATE || $var_type === VEG_PARAM_DATETIME) {
			$var = strtotime($var);
			if ($var) {
				$var = date($var_type === VEG_PARAM_DATE ? 'Y-m-d' : 'Y-m-d H:i:s', $var);
			}
		} elseif ($var_type === VEG_PARAM_EMAIL) {
			$var = FV::validEmail($var);
		} elseif ($var_type === VEG_PARAM_PHONE) {
			$var = FV::validPhone($var);
		} elseif ($var_type === VEG_PARAM_EDIT) {
			$var = FT::prepEdit($var);
		} elseif ($var_type) {
			throw new Exception('F::get type is undefined ' . $var_type);
		} elseif ($is_string) {
			$var = self::strip($var);
		}
		return !$var ? $var_default : $var;
	}

	private static function strip($var) {
		$var = trim(strip_tags($var));
		if (strtoupper($var) ==='NULL') {
			$var = '';
		}
		return $var;
	}
	private static function stripArray($var) {
		foreach ($var AS &$subvar) {
			if (is_array($subvar)) {
				$subvar = self::stripArray($subvar);
			} else {
				$subvar = self::strip($subvar);
			}
		}
		return $var;
	}


	/**
	 * @return  string  client ip
	 */
	static public function ip()
	{
		global $_SERVER, $HTTP_SERVER_VARS;
		$REMOTE_ADDR = '';
		if (isset($_SERVER['HTTP_X_REAL_IP'])) {
			$REMOTE_ADDR = $_SERVER['HTTP_X_REAL_IP'];
		} elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$REMOTE_ADDR = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} elseif(isset($_SERVER['REMOTE_ADDR'])) {
			$REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];
		} elseif (isset($HTTP_SERVER_VARS['REMOTE_ADDR'])) {
			$REMOTE_ADDR = $HTTP_SERVER_VARS['REMOTE_ADDR'];
		} elseif (getenv('REMOTE_ADDR')) {
			$REMOTE_ADDR = getenv('REMOTE_ADDR');
		}
		if (!$REMOTE_ADDR) {
			return false;
		}
		$REMOTE_ADDR = preg_replace('/[^0-9.]/', '', $REMOTE_ADDR);
		if (!filter_var($REMOTE_ADDR, FILTER_VALIDATE_IP)) {
			return false;
		}
		return $REMOTE_ADDR;
	}

	/**
	 * @return  string  HTTP REFERER
	 */
	static public function referer()
	{
		if (!isset($_SERVER['HTTP_REFERER'])) {
			return false;
		}
		$ref = $_SERVER['HTTP_REFERER'];
		if (!$ref || !is_string($ref)) {
			return false;
		}
		return $ref;
	}
	
	static public function ajaxed()
	{
		return F::get('is_ajaxed') || @$_SERVER['HTTP_X_REQUESTED_WITH'] ==='XMLHttpRequest';
	}

	/**
	 * @return  string  AGENT
	 */
	static public function agent()
	{
		if (!isset($_SERVER['HTTP_USER_AGENT'])) {
			return false;
		}
		$ref = $_SERVER['HTTP_USER_AGENT'];
		if (!$ref || !is_string($ref)) {
			return false;
		}
		return $ref;
	}

	/**
	 * @return  string  REQUEST_URI
	 */
	static public function uri()
	{
		if (!isset($_SERVER['REQUEST_URI'])) {
			return false;
		}
		$uri = $_SERVER['REQUEST_URI'];
		if (!$uri || !is_string($uri)) {
			return false;
		}
		return $uri;
	}
}
