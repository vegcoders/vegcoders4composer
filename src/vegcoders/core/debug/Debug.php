<?php
namespace vegcoders\core\debug;

use vegcoders\core\basic\BasicSingleLogged;
use vegcoders\db_tables\logs\Debug AS DebugOne;
/**
 * Class Debug
 * @package vegcoders\core\debug
 */
class Debug extends BasicSingleLogged
{
	protected static $_instance;
	protected static $_can_debug;
	protected static $_can_debug_collect;


	/**
	 * show visual difference between two variables
	 * @param $var1
	 * @param $var2
	 */
	static public function visual($var1, $var2)
	{
		echo '<table><tr><td valign="top"><pre>';
		/** @noinspection ForgottenDebugOutputInspection */
		var_export($var1);
		echo '</pre></td><td valign="top"><pre>';
		/** @noinspection ForgottenDebugOutputInspection */
		var_export($var2);
		echo '</pre></td></tr></table>';
	}

	/**
	 * put into debug table
	 * @param string $var - title
	 * @param string | array $val - value
	 * @param string | array $val2 - sub value
	 * @return array|bool
	 * @throws \Exception
	 */
	static public function put($var, $val, $val2 = '')
	{
		$debug = new DebugOne(array(
			'title' 		=> $var,
			'text' 		=> is_string($val) ? $val : var_export($val, true),
			'text2' 	=> is_string($val2) ? $val2 : var_export($val2, true)
		));
		return $debug->insert();
	}

	/**
	 * @param $code
	 * @param $str
	 * @param bool $show_debug
	 * @return array|bool
	 * @throws \Exception
	 */
	static public function err($code, $str, $show_debug = true)
	{
		if ($show_debug && defined('DEBUG') && DEBUG) {
			echo '<h2>' . date('H:i:s') . ' ' . $code . '</h2> ' . $str;
			return true;
		}
		return self::put('error', $code, $str);
	}

	/**
	 * @param $object
	 * @param $var
	 * @param $val
	 * @return bool
	 */
	static public function inner($object, $var, $val)
	{
		self::_log('<b>' . $object. '</b> ' . Error::s($var) . ' => ' . Error::s($val), 'blue');
		return true;
	}
}
