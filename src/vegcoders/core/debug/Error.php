<?php
namespace vegcoders\core\debug;

use vegcoders\core\cache\Cache;
use vegcoders\core\db\DB;
use vegcoders\core\funcs\F;
use vegcoders\db_tables\logs\Error AS ErrorOne;

use Exception;
use AppSiteSettings;

class Error
{
	static public function getUserMessage(Exception $e)
	{
		$message = $e->getMessage();
		if (AppSiteSettings::DEBUG) {
			$full =
				"<br/>\nIP:\t " . F::ip()
				. "<br/>\nTIME:\t " . date('Y-m-d H:i:s')
				. "<br/>\nURI:\t " . @$_SERVER['REQUEST_URI']
				. "<br/>\nPOST:\t " . str_replace('&amp;', '&', http_build_query($_POST))
				. "<br/>\nMessage:\t " . $message
				. "<br/>\nFile:\t " . $e->getFile() . ':' . $e->getLine()
				. "<br/>\nTrace:\t\n<br/>" . str_replace("\n", "\n<br/>", $e->getTraceAsString())
				. "<br/>\nDB:\t " . DB::debug();
			echo $full;
			return true;
		}
		$cache = 'error_' . md5($message);
		$time = Cache::get($cache);
		if ($time && time() - $time < 600) {
			return true;
		}
		Cache::put($cache, time());

		$full = "Trace:\t\n<br/>" . str_replace("\n", "\n<br/>", $e->getTraceAsString());
		if ($_POST) {
			$full .= "<br/>\nPOST:\t " . str_replace('&amp;', '&', http_build_query($_POST));
		}
		if ($_GET) {
			$full .= "<br/>\nGET:\t " . str_replace('&amp;', '&', http_build_query($_GET));
		}
		$json = F::getAngular();
		if ($json) {
			$full .= "<br/>\nANGULAR:\t " . var_export($json, true);
		}
		$full .= "<br/>\nDB:\t " . DB::debug();
		$error = new ErrorOne(array(
			'code' 			=> $e->getCode(),
			'title' 		=> $message,
			'source' 		=> $e->getFile() . ':' . $e->getLine(),
			'description' 	=> $full
		));
		$error->insert();
		if ($e->getCode() > 0) {
			echo 'Простите, произошла системная ошибка! Попробуйте позже.';
			return true;
		}
		if (!F::ajaxed()) {
			header('Location:' . AppSiteSettings::FRONT_SITE_URL);
			exit;
		}
		die('Exception was caught');
	}
	static public function s($var) {
		if (is_int($var) || is_string($var)) {
			return $var;
		}
		return json_encode($var);
	}
}
