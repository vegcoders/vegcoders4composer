<?php
namespace vegcoders\core\basic;
/**
 * Класс "неклонируемый" для наследования во всех классах с необходимостью запретов клонирования
 */
class BasicSingle
{
	/**
	 * @var BasicSingle for php>5.3 version
	 * заметь! для всех классов отнаследованных - надо добавлять свой собственный объект "инстанс"
	 */
	protected static $_instance;
	/**
	 * @var array
	 */
	private static $_params;

	/**
	 * для времени запуска
	 */
	protected $_start_time;

	/**
	 * защищаем от создания через new
	 */
	protected function __construct()
	{
	}

	/**
	 * защищаем от создания через клонирование
	 */
	private function __clone()
	{
	}

	/**
	 * для добавления функций к созданию объекта
	 * @param array $params - Общий массив параметров
	 * @return bool
	 */
	public function _afterConstruction($params)
	{
		self::$_params = $params;
		return true;
	}

	/**
	 * возвращает единственный экземпляр класса (наследника!)
	 * @param array $params - Общий массив параметров
	 * @return BasicSingle
	 */
	static public function getInstance(array $params = array())
	{
		if (!self::checkIfCreated()) {
			static::$_instance = new static();
			static::$_instance->_start_time = microtime(true);
			static::$_instance->_afterConstruction($params);
		}
		return static::$_instance;
	}

	/**
	 * @return boolean
	 */
	static public function checkIfCreated()
	{

		return null !== static::$_instance;
	}
}