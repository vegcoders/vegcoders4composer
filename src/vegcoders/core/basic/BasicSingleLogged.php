<?php
namespace vegcoders\core\basic;

use AppSiteSettings;
/**
 * Класс "неклонируемый" c логом
 */
class BasicSingleLogged extends BasicSingle
{
	protected static $_instance;
	/**
	 * дебаг режим
	 * @var boolean
	 */
	protected static $_can_debug 			= false;
	/**
	 * собирать ли все в лог
	 * @var boolean
	 */
	protected static $_can_debug_collect 	= AppSiteSettings::DEBUG;

	public     $debug_log 		= '';		//краткий лог (от последнего вывода)
	public	   $debug_log_full = '';		//полный лог запросов
	public     $debug_shown 	= false;	//показывалась ли уже запись о запросах (например была ошибка)

	/**
	 * была ли ошибка в этом сеансе
	 * @var boolean
	 */
	public 		$has_error = false;

	/**
	 * в конце работы если включен can_debug выводить данные всего лога
	 */
 	public function __destruct()
	{
		$c = get_called_class();
		if (static::$_can_debug && static::$_can_debug_collect && $this->debug_log && !@$_SERVER['HTTP_X_REQUESTED_WITH']) {
			if ($this->debug_shown) {
				echo '<div style="position:absolute;top:0;z-index:200000;">
					<a href="#" onclick="$(this).next().toggle(); return false;">[+' . $c . ']</a>
					<div style="display:none;background:#ffffff;border:1px solid blue;padding:5px;">';
			} else {
				echo '<h2>' . $c . '</h2>';
			}
			echo $this->debug_log;
			if ($this->debug_shown) {
				echo '</div></div>';
			}
		}
	}

	/**
	 * записывать строки в лог
	 * @param 	string $str
	 * @param	string $color
	 * @return 	bool
	 */
	protected static function _log($str, $color = '')
	{
		if (!static::$_can_debug_collect) {
			return false;
		}
		if ($color) {
			$str = '<div style="color:' . $color . '">' . $str . '</div>';
		}
		/** @var BasicSingleLogged $object */
		$object = self::getInstance();
		$object->debug_log .= $str . '<br/>';
		if (!@$_SERVER['IDE_PHPUNIT_VERSION']) {
			ob_start();
			/** @noinspection ForgottenDebugOutputInspection */
			debug_print_backtrace();
			$ob = ob_get_contents();
			ob_end_clean();
			$object->debug_log .= '<a href="#" onclick="$(this).next().toggle(); return false;">trace prev log</a>'
				. '<pre style="display:none;">' . $ob .	'</pre><br/><br/>';
		}
		return true;
	}

	/**
	 * возвращаем лог действий для вывода, сам выведенный кусок лога сохраняется в полном логе
	 * @return bool|string
	 */
	static public function debug()
	{
		if (@$_SERVER['HTTP_X_REQUESTED_WITH']) {
			return false;
		}
		if (!static::$_can_debug_collect) {
			return 'Turn on if DEBUG_COLLECT';
		}
		if (!self::checkIfCreated()) {
			return false;
		}
		/** @var BasicSingleLogged $object */
		$object = self::getInstance();
		$tmp    = $object->debug_log;
		$object->debug_log_full .= $tmp;
		$object->debug_log 		= '';
		$object->debug_shown 	= true;
		return $tmp;
	}

	static public function fullDebug()
	{
		if (!self::checkIfCreated()) {
			return false;
		}
		/** @var BasicSingleLogged $object */
		/** @noinspection OneTimeUseVariablesInspection */
		$object = self::getInstance();
		return $object->debug_log_full;
	}
	
	/**
	 * меняем параметры "режима" (логгировать или нет действия)
	 * @param   bool    $debug
	 * @return  bool
	 */
	static public function setDebug($debug) {
		static::$_can_debug = $debug;
		static::$_can_debug_collect = $debug;
		return true;
	}
	/**
	 * узнаем параметры режима
	 * @return  bool
	 */
	static public function getDebug() {
		return static::$_can_debug;
	}
	/**
	 * очищаем лог действий без вывода
	 * @return bool
	 */
	static public function clearDebug()
	{
		if (!self::checkIfCreated()) {
			return false;
		}
		/** @var BasicSingleLogged $object */
		$object = self::getInstance();
		$object->debug_log_full = '';
		$object->debug_log 		= '';
		$object->debug_shown 	= true;
		return true;
	}
}	