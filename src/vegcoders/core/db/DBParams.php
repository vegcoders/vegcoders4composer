<?php
namespace vegcoders\core\db;

class DBParams
{
	public $host;
	public $db;
	public $user;
	public $pass;
	public function __construct($params)
	{
		if (!isset($params['host'])) {
			throw new \Exception ('host is needed');
		}
		if (!isset($params['db'])) {
			throw new \Exception ('db is needed');
		}
		if (!isset($params['user'])) {
			throw new \Exception ('user is needed');
		}
		if (!isset($params['pass'])) {
			throw new \Exception ('pass is needed');
		}
		$this->host = $params['host'];
		$this->db 	= $params['db'];
		$this->user = $params['user'];
		$this->pass = $params['pass'];
	}
}