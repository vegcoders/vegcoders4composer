<?php
namespace vegcoders\core\db\subs;

use vegcoders\core\db\DB;
use vegcoders\core\db\engines\PostgreDB;
use vegcoders\core\debug\Error;
use Exception;

trait DBInsert
{
	/**
	 * @param string $table
	 * @param array $row
	 * @param string $return_id
	 * @return mixed
	 * @throws Exception
	 */
	static public function insert($table, $row, $return_id = 'id')
	{
		if (!$row) {
			throw new Exception('DB cant create empty row insert sql', VEG_ERROR_SYSTEM_DB);
		}
		if (!is_array($row)) {
			throw new Exception('DB cant create not array row insert sql: ' . Error::s($row), VEG_ERROR_SYSTEM_DB);
		}
		
		/** @var DB $db_object */
		$db_object = self::getInstance();
		/** @var PostgreDB $db_engine */
		$db_engine = $db_object->getEngine();
		return $db_engine->_insert($table, $row, $return_id);
	}

	static public function insertMulti($table, $rows, $return_id = 'id')
	{
		if (!$rows) {
			throw new Exception('DB cant create empty row insert Multi sql', VEG_ERROR_SYSTEM_DB);
		}
		if (!is_array($rows)) {
			throw new Exception('DB cant create not array row insert Multi sql: ' . Error::s($rows), VEG_ERROR_SYSTEM_DB);
		}		
		/** @var DB $db_object */
		$db_object = self::getInstance();
		/** @var PostgreDB $db_engine */
		$db_engine = $db_object->getEngine();
		return $db_engine->_insertMulti($table, $rows, $return_id);
	}
	
}