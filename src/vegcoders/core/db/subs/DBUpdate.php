<?php
namespace vegcoders\core\db\subs;

use Exception;
use vegcoders\core\db\DB;
use vegcoders\core\db\engines\PostgreDB;

trait DBUpdate
{

	/**
	 * @param string $table
	 * @param string|int $id
	 * @param array $row
	 * @return mixed
	 * @throws Exception
	 */
	static public function update($table, $id, $row)
	{
		if (!$id) {
			throw new Exception('DB cant create with no id update sql', VEG_ERROR_SYSTEM_DB);
		}
		if (!$row) {
			throw new Exception('DB cant create empty row update sql', VEG_ERROR_SYSTEM_DB);
		}

		/** @var DB $db_object */
		$db_object = self::getInstance();
		/** @var PostgreDB $db_engine */
		$db_engine = $db_object->getEngine();
		return $db_engine->_update($table, $id, $row);
	}
}