<?php
namespace vegcoders\core\db;

use vegcoders\core\basic\BasicSingleLogged;
use vegcoders\core\db\engines\PostgreDB;
use vegcoders\core\db\subs\DBInsert;
use vegcoders\core\db\subs\DBUpdate;
use Exception;
use AppEnginesSettings;
/**
 * Класс единой базы данных, разные подключения через "двигатель"
 */
class DB extends BasicSingleLogged
{
	use DBInsert;
	use DBUpdate;
	
	const RETURN_ARRAY = 'array';
	const RETURN_LINK = 'link';
	const RETURN_LIST = 'list';
	const RETURN_MLIST = 'mlist';
	const RETURN_MMLIST = 'mmlist';
	const RETURN_ALIST = 'alist';
	const RETURN_ONE = 'one';

	const POSTGRE_ENGINE = 'PostgreDB';
	const MYSQL_ENGINE = 'MysqlDB';

	protected static $_instance;
	protected static $_can_debug;
	protected static $_can_debug_collect;

	/**
	 * все открытые соединения (на один тип движка можно сколько угодно соединений)
	 * @var array
	 */
	public $_db_engine = array();

	/**
	 * карта перевода имен соединений в исползованные классы
	 * @var array
	 */
	protected static $_db_engine_map_to_classes = array();
	/**
	 * параметры соединений
	 * @var array
	 */
	protected static $_db_engine_params = array();

	protected static $_db_default_engine = '';

	public function __destruct()
	{
		parent::__destruct();
		$this->_close();
	}

	private function _close()
	{
		if ($this->_db_engine) {
			foreach ($this->_db_engine AS $db_engine) {
				$db_engine->_close();
			}
		}
	}
	public static function close()
	{
		if (!self::checkIfCreated()) {
			return false;
		}
		/** @var DB $db_object */
		$db_object = self::getInstance();
		return $db_object->_close();
	}

	public static function setDefaultEngine($db_name = 'default')
	{
		self::$_db_default_engine = $db_name;
	}

	public function getEngine($db_name = '')
	{
		/** @var DB $db_object */
		$db_object = self::getInstance();

		if (!$db_name) {
			if (!self::$_db_default_engine) {
				self::$_db_default_engine = 'default';
			}
			$db_name = self::$_db_default_engine;
		}

		if (!isset(self::$_db_engine_map_to_classes[$db_name])) {
			if ($db_name === 'default') {
				self::$_db_engine_map_to_classes[$db_name] = AppEnginesSettings::CORE_DB_ENGINE;
			} else {
				throw new Exception('class for ' . $db_name . ' is undefined');
			}
		}
		$db_class = self::$_db_engine_map_to_classes[$db_name];

		$db_params = null;
		if (isset(self::$_db_engine_params[$db_name])) {
			$db_params = self::$_db_engine_params[$db_name];
		}
		if (!isset($db_object->_db_engine[$db_name])) {
			self::_log('engine:' . $db_name);
			$db_class_full = 'vegcoders\\core\\db\\engines\\' . $db_class;
			$start_time = microtime(true);
			/** @var PostgreDB $db_engine */
			$db_engine = new $db_class_full();
			$db_engine->_open($db_name, $db_params);
			$opened_time = microtime(true);
			$exec_time = round($opened_time - $start_time, 4);
			self::_log($exec_time . ' sec init ' . $db_name . ' db connection', 'blue');
			$db_object->_db_engine[$db_name] = $db_engine;
		}
		return $db_object->_db_engine[$db_name];
	}

	/**
	 * инициализируем движок, но физически не создаем еще
	 * @param string $db_class имя класса из констант (постре, мускул)
	 * @param string $db_name псевдоним соединения для переключений между несколькими (если одна база можно забить)
	 * @param DBParams $params параметры соединения
	 * @throws Exception
	 */
	static public function initEngine($db_class, $db_name = '', DBParams $params = null)
	{
		if (!$db_name) {
			$db_name = $db_class;
		}
		self::$_db_default_engine = $db_name;
		self::$_db_engine_map_to_classes[$db_name] = $db_class;
		if ($params) {
			if (isset(self::$_db_engine_params[$db_name])) {
				throw new Exception ('params for connection ' . $db_class . ' ' . $db_name . ' have been already setted');
			}
			self::$_db_engine_params[$db_name] = $params;
		}
	}

	static public function removeEngine($db_name)
	{
		if (!isset(self::$_db_engine_map_to_classes[$db_name])) {
			throw new Exception ('init for connection ' . $db_name . ' is not setted to remove');
		}
		/** @var DB $db_object */
		$db_object = self::getInstance();
		if (isset($db_object->_db_engine[$db_name])) {
			/** @var PostgreDB $db_engine */
			$db_engine = $db_object->_db_engine[$db_name];
			$db_engine->_close();
			unset($db_object->_db_engine[$db_name]);
		}
		unset(self::$_db_engine_map_to_classes[$db_name], self::$_db_engine_params[$db_name]);
		self::setDefaultEngine();
		return true;
	}

	static public function begin($db_name = '')
	{
		/** @var DB $db_object */
		$db_object = self::getInstance();
		$db_object ->getEngine($db_name)->_begin();
		self::_log('BEGIN WORK', 'blue');
	}

	static public function commit($db_name = '')
	{
		/** @var DB $db_object */
		$db_object = self::getInstance();
		$db_object->getEngine($db_name)->_commit();
		self::_log('COMMIT', 'blue');
	}

	static public function rollback($db_name = '')
	{
		/** @var DB $db_object */
		$db_object = self::getInstance();
		$db_object->getEngine($db_name)->_rollback();
		self::_log('ROLLBACK', 'red');
	}
	static public function truncate($table, $db_name = '')
	{
		/** @var DB $db_object */
		$db_object = self::getInstance();
		$db_object->getEngine($db_name)->_truncate($table);
		self::_log('TRUNCATED ' . $table, 'red');
	}


	/**
	 * @param array|string $query запрос
	 * @param string $return что вернуть
	 * @param string $db_name псевдоним соединения
	 * @return array|bool|string что угодно по типу запроса
	 * @throws Exception
	 */
	static public function query($query, $return = '', $db_name = '')
	{
		if (is_array($query)) {
			self::begin($db_name);
			foreach ($query AS $ones) {
				if (is_array($ones)) {
					/** @noinspection ForeachSourceInspection */
					foreach ($ones AS $one) {
						$one = trim($one);
						if ($one && !self::query($one, $return)) {
							self::rollback($db_name);
							return false;
						}
					}
				} else {
					$one = trim($ones);
					if ($one && !self::query($one, $return, $db_name)) {
						self::rollback();
						return false;
					}
				}
			}
			self::commit($db_name);
			return true;
		}

		/** @var DB $db_object */
		$db_object = self::getInstance();
		/** @var PostgreDB $db_engine */
		$db_engine = $db_object->getEngine($db_name);

		$start_time = microtime(true);
		$link = $db_engine->_query($query);
		$mtime = microtime(true);
		$exec_time = round($mtime - $start_time, 4);
		if (!$link) {
			$back = debug_backtrace();
			self::_log($exec_time . ' sec <br/>' . htmlentities($query) . '<br/>'
				. $db_engine->_error() . '<Br/>' . $back[0]['file'] . ':' . $back[0]['line'], 'red');
			$db_object->has_error = true;
			return false;
		}
		self::_log($exec_time . ' sec <b></b> ' . $db_engine->_numRows($link) . ' rows ' . htmlentities($query), 'green');

		if (!$return && stripos(trim($query), 'select') === 0) {
			$return = self::RETURN_ONE;
		}
		$result = $db_object->_formatResult($db_engine, $link, $return);
		if (!is_resource($result)) {
			$db_engine->_closeLink($link);
		}
		return $result;
	}

	/**
	 * @param $query
	 * @return array
	 * @throws \Exception
	 */
	public static function queryArray($query)
	{
		return self::query($query, self::RETURN_ARRAY);
	}

	/**
	 * @param $query
	 * @return array
	 * @throws \Exception
	 */
	public static function queryListArray($query)
	{
		return self::query($query, self::RETURN_LIST);
	}

	/**
	 * @param $query
	 * @return array
	 * @throws \Exception
	 */
	public static function queryMultiListArray($query)
	{
		return self::query($query, self::RETURN_MLIST);
	}

	/**
	 * @param $link
	 * @return array
	 * @throws \Exception
	 */
	public static function linkArray($link)
	{
		/** @var DB $db_object */
		$db_object = self::getInstance();
		/** @var PostgreDB $db_engine */
		$db_engine = $db_object->getEngine();
		return $db_engine->_fetchArray($link);
	}

	/**
	 * @param PostgreDB $db_engine движок для перебора результатов
	 * @param resource $link ссылка на результат запроса
	 * @param string $return тип возвращаемых данных
	 * @return boolean | string | array
	 * @throws Exception
	 * @noinspection MoreThanThreeArgumentsInspection */
	private function _formatResult($db_engine, $link, $return)
	{
		if ($return === self::RETURN_LINK) {
			return $link;
		}

		if ($return === self::RETURN_ARRAY) {
			$result = array();
			while ($row = $db_engine->_fetchArray($link)) {
				$result[] = $row;
			}
			return $result;
		}
		
		if ($return === self::RETURN_ALIST) {
			$result = array();
			if ($row = $db_engine->_fetchArray($link)) {
				$fkey = array_keys($row);
				$fkey = array_shift($fkey);
				do {
					$result[$row[$fkey]] = $row;
				} while ($row = $db_engine->_fetchArray($link));
			}
			return $result;
		}

		if ($return === self::RETURN_LIST || $return === self::RETURN_MLIST || $return === self::RETURN_MMLIST) {
			$result = array();
			$func_name = ($return === self::RETURN_LIST) ? '_fetchRow' : '_fetchArray';
			$row = $db_engine->$func_name($link);
			if (!$row) {
				return $result;
			}

			$cn = count($row);
			if ($cn === 0) {
				throw new Exception('add more fields (more then 0) is needed');
			}
			/** @noinspection NotOptimalIfConditionsInspection */
			if ($return === self::RETURN_MMLIST) {
				if ($cn <= 2) {
					throw new Exception('add more fields (more then 2) is needed');
				}
				if ($cn === 3) {
					$func_name = '_fetchRow';
					$row = array_values($row);
					do {
						$key = $row[0];
						if (!$key) {
							$key = 0;
						}
						$val = $row[1];
						if (!$val) {
							$val = 0;
						}
						$result[$key][$val][$row[2]] = $row[2];
					} while ($row = $db_engine->$func_name($link));
				} else {
					do {
						$result[array_shift($row)][array_shift($row)][] = $row;
					} while ($row = $db_engine->$func_name($link));
				}
				return $result;
			}

			/** @noinspection NotOptimalIfConditionsInspection */
			if ($return === self::RETURN_MLIST) {
				if ($cn <= 1) {
					throw new Exception('add more fields (more then 1) is needed');
				}
				if ($cn === 2 || $cn === 3) {
					$func_name = '_fetchRow';
					$row = array_values($row);
					do {
						$key = $row[0];
						if (!$key) {
							$key = 0;
						}
						$val = $row[1];
						if (!$val) {
							$val = 0;
						}
						/** @noinspection NotOptimalIfConditionsInspection */
						if ($cn === 2) {
							$set_val = $val;
						} else {
							$set_val = $row[2];
						}
						$result[$key][$val] = $set_val;
					} while ($row = $db_engine->$func_name($link));
				} else {
					do {
						$result[array_shift($row)][] = $row;
					} while ($row = $db_engine->$func_name($link));
				}
				return $result;
			}


			if ($cn === 1) {
				do {
					$result[$row[0]] = $row[0];
				} while ($row = $db_engine->_fetchRow($link));
			} elseif ($cn === 2) {
				do {
					$result[$row[0]] = $row[1];
				} while ($row = $db_engine->_fetchRow($link));
			} else {
				do {
					$result[array_shift($row)] = $row;
				} while ($row = $db_engine->$func_name($link));
			}
			return $result;

		}

		if ($return === self::RETURN_ONE) {
			$row = $db_engine->_fetchArray($link);
			$result = $row;
			if ($row && count($row) === 1) {
				$result = array_pop($row);
			}
			return $result;
		}

		if (is_object($return) || function_exists($return)) {
			$result = array();
			while ($row = $db_engine->_fetchArray($link)) {
				$return($result, $row);
			}
			return $result;
		}

		return $link;
	}
}