<?php
namespace vegcoders\core\db;

use vegcoders\core\funcs\FV;
/**
 * Класс всяких функций к базе данных для убыстрения работы
 */
class DBHelper
{
	static public function arrayToCondition($ids, $as_int = false)
	{
		if ($as_int) {
			return FV::intsToSql($ids);
		}
		return is_array($ids)
			? (
				' IN (' . implode(',', self::escape($ids, true)) . ')'
			)
			: (
				'=' . self::escape($ids, true)
			);
	}

	
	static public function escape($str_or_array, $add_quotes = false)
	{
		if (is_array($str_or_array)) {
			$result = array();
			foreach ($str_or_array AS $rid => $name) {
				$result[$rid] = self::escape($name, $add_quotes);
			}
			return $result;
		}
		$str_or_array = trim($str_or_array);
		$lower = mb_strtolower($str_or_array);
		if ($lower === 'now()' || $lower === 'null') {
			return $str_or_array;
		}
		$tmp = addslashes($str_or_array);
		if ($add_quotes) {
			$tmp = '\'' . $tmp . '\'';
		}
		return $tmp;
	}
}