<?php
namespace vegcoders\core\db\engines;

use Exception;
use vegcoders\core\db\DBHelper;
use vegcoders\core\db\DBParams;
use AppSiteSettings;
use vegcoders\core\debug\Error;

class MysqlDB extends EngineDB
{
	private $_trans_started = false;

	/**
	 * @var \mysqli
	 */
	private $_db_link;
	public function _open($db_name = AppSiteSettings::DB_NAME, DBParams $params = null)
	{
		$this->_settings($db_name, $params);
		$this->_db_link = new \mysqli($this->_db_host, $this->_db_user, $this->_db_pass, $this->_db_name);
		if ($this->_db_link->connect_error) {
			throw new Exception($this->_db_link->connect_error, VEG_ERROR_SYSTEM_DB);
		}
		$this->_db_link->set_charset('utf8');
	}

	public function _close()
	{
		if (!$this->_db_link) {
			return false;
		}
		$this->_db_link->close();
		return true;
	}

	public function _escape($str)
	{
		if ($this->_db_link) {
			return $this->_db_link->real_escape_string(trim($str));
		}

		return addslashes(trim($str));
	}

	public function _begin()
	{
		if (!$this->_db_link) {
			throw new Exception('DB TRANSACTION BEGIN NO LINK', VEG_ERROR_SYSTEM_DB);
		}
		if ($this->_trans_started) {
			throw new Exception('DB TRANSACTION STARTED', VEG_ERROR_SYSTEM_DB);
		}
		$this->_trans_started = true;
		$this->_db_link->autocommit(FALSE);
		if(phpversion()>'5.5.0' && !$this->_db_link->begin_transaction()) {
			throw new Exception('bad begin of transaction');
		}
	}

	public function _commit()
	{
		if (!$this->_db_link) {
			throw new Exception('DB TRANSACTION COMMIT NO LINK', VEG_ERROR_SYSTEM_DB);
		}
		if (!$this->_trans_started) {
			throw new Exception('DB TRANSACTION NOT STARTED', VEG_ERROR_SYSTEM_DB);
		}
		$this->_trans_started = false;
		$this->_db_link->commit();
		$this->_db_link->autocommit(TRUE);
	}

	public function _rollback()
	{
		if (!$this->_db_link) {
			throw new Exception('DB TRANSACTION ROLLBACK NO LINK', VEG_ERROR_SYSTEM_DB);
		}
		if (!$this->_trans_started) {
			throw new Exception('DB TRANSACTION NOT STARTED', VEG_ERROR_SYSTEM_DB);
		}
		$this->_trans_started = false;
		$this->_db_link->rollback();
		$this->_db_link->autocommit(TRUE);
	}

	public function _query($query)
	{
		try {
			if (!$query) {
				return true;
			}
			$result = $this->_db_link->query($query);
			if (!$result) {
				$error = $this->_db_link->error;
				throw new Exception('DB connection: ' . $this->_db_name . '<br/>' . PHP_EOL . 'DB error:' . $error . '<br/>' . PHP_EOL . $query);
			}
			return $result;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function _error()
	{
		return $this->_db_link->error;
	}

	/**
	 * @param \mysqli_result $link
	 * @return int|string
	 * @throws \Exception
	 */
	public function _numRows($link)
	{
		if ($link === true) {
			return 1;
		}
		if (!is_object($link)) {
			throw new Exception('not valid result ' . Error::s($link));
		}
		return (int)@$link->num_rows;
	}

	/**
	 * @param \mysqli_result $link
	 * @return array
	 */
	public function _fetchAll($link)
	{
		return $link->fetch_all();
	}

	/**
	 * @param \mysqli_result $link
	 * @return mixed
	 */
	public function _fetchArray($link)
	{
		return $link->fetch_assoc();
	}

	/**
	 * @param \mysqli_result $link
	 * @return mixed
	 */
	public function _fetchRow($link)
	{

		return $link->fetch_row();
	}
	public function _truncate($table)
	{
		return $this->_query('TRUNCATE TABLE ' . DBHelper::escape($table));
	}

	public function _insert($table, $row, $return_id)
	{
		$query = $this->_insertSql($table, $row);
		$actually_bool_result  = $this->_query($query);
		if (!$actually_bool_result) {
			return false;
		}
		if (!$return_id) {
			return true;
		}
		$row = $this->_db_link->insert_id;
		if (!$row) {
			return false;
		}
		return $row;
	}

	public function _insertMulti($table, array $rows, $return_id)
	{
		if (!$return_id) {
			$query = $this->_insertMultiSql($table, $rows);
			return $this->_query($query);
		}

		$ids = array();
		foreach ($rows AS $row) {
			$id = (int) $this->_insert($table, $row, true);
			if (!$id) {
				return false;
			}
			$ids[$id] = $id;
		}
		return $ids;
	}
}