<?php
namespace vegcoders\core\db\engines;

use vegcoders\core\db\DBHelper;
use vegcoders\core\db\DBParams;
use Exception;
use AppSiteSettings;

class EngineDB
{
	protected $_db_name = '';
	protected $_db_host = '';
	protected $_db_user = '';
	protected $_db_pass = '';

	protected function _settings($db_name = AppSiteSettings::DB_NAME, DBParams $params = null)
	{
		if ($params && $params->db) {
			$this->_db_name = $params->db;
		} elseif ($db_name) {
			$this->_db_name = $db_name;
		}
		if (!$this->_db_name || $this->_db_name === 'default') {
			$this->_db_name = AppSiteSettings::DB_NAME;
		}

		$this->_db_host = AppSiteSettings::DB_HOST;
		if ($params && $params->host) {
			$this->_db_host = $params->host;
		}

		$this->_db_user = AppSiteSettings::DB_USER;
		if ($params && $params->user) {
			$this->_db_user = $params->user;
		}

		$this->_db_pass = AppSiteSettings::DB_PASS;
		if ($params && $params->pass) {
			$this->_db_pass = $params->pass;
		}
	}
	public function _insert($table, $row, $return_id)
	{
		$query = $this->_insertSql($table, $row);
		if ($return_id) {
			$query .= ' RETURNING ' . $return_id;
		}
		$link  = $this->_query($query);
		if (!$link) {
			return false;
		}
		if (!$return_id) {
			return true;
		}
		$row = $this->_fetchRow($link);
		if (!$row || !is_array($row) || !isset($row[0])) {
			return false;
		}
		$this->_closeLink($link);
		return $row[0];
	}

	public function _insertMulti($table, array $rows, $return_id)
	{
		$query = $this->_insertMultiSql($table, $rows);
		if ($return_id) {
			$query .= ' RETURNING ' . $return_id;
		}
		$link = $this->_query($query);
		if (!$link) {
			return false;
		}
		if (!$return_id) {
			return true;
		}
		$row = $this->_fetchRow($link);
		if (!$row) {
			return false;
		}
		$ids = array($row[0] => $row[0]);
		while ($row = $this->_fetchRow($link))
		{
			$ids[$row[0]] = $row[0];
		}
		$this->_closeLink($link);
		return $ids;
	}

	protected function _insertSql($table, $row)
	{

		$sql  = ' INSERT INTO ' . $table . ' (';
		$sql2 = '';
		foreach ($row AS $var => $val) {
			if ($sql2) {
				$sql  .= ', ';
				$sql2 .= ', ';
			}
			$sql .= $var;
			$sql2 .= DBHelper::escape($val, true);
		}
		$sql .= ') VALUES ( ' . $sql2 . ')';
		return $sql;
	}

	protected function _insertMultiSql($table, array $rows)
	{
		$first_row = array_shift($rows);
		$sql = $this->_insertSql($table, $first_row);
		if (!$rows) {
			return $sql;
		}
		$columns = array_keys($first_row);
		foreach ($rows AS $row) {
			$sql_2 = '';
			foreach ($columns AS $column) {
				if ($sql_2) {
					$sql_2 .= ',';
				}
				$val = @$row[$column];
				$sql_2 .= DBHelper::escape($val, true);
			}
			$sql .= ', (' . $sql_2 . ')';
		}
		return $sql;
	}



	public function _update($table, $edit_id, $row)
	{
		$query = $this->_updateSql($table, $edit_id, $row);
		$link  = $this->_query($query);
		if (!$link) {
			return false;
		}
		$this->_closeLink($link);
		return true;
	}

	/**
	 * @param $table
	 * @param $edit_id
	 * @param $row
	 * @return string
	 * @throws Exception
	 */
	public function _updateSql($table, $edit_id, $row)
	{
		$sql = ' UPDATE ' . $table . ' SET ';
		$i = 0;
		if (is_array($row)) {
			foreach ($row AS $var => $val) {
				if ($i > 0) {
					$sql .= ', ';
				}
				$i++;
				$sql .= $var . '=' . DBHelper::escape($val, true);
			}
		} else {
			$sql .= $row;
		}
		$int = (int) $edit_id;
		if ($int) {
			$sql .= ' WHERE id=' . $int;
		} else {
			$sql .= ' ' . $edit_id;
		}
		return $sql;
	}

	public function _query(/** @noinspection PhpUnusedParameterInspection */
		$query)
	{
		return [];
	}

	public function _fetchAll(/** @noinspection PhpUnusedParameterInspection */
		$link)
	{
		return [];
	}

	public function _fetchArray(/** @noinspection PhpUnusedParameterInspection */
		$link)
	{
		return [];
	}

	public function _fetchRow(/** @noinspection PhpUnusedParameterInspection */
		$link)
	{
		return [];
	}
	public function _closeLink(/** @noinspection PhpUnusedParameterInspection */
		$link)
	{
		return true;
	}
}
