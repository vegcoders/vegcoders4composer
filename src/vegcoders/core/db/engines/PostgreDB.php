<?php
namespace vegcoders\core\db\engines;

use Exception;
use vegcoders\core\db\DBHelper;
use vegcoders\core\db\DBParams;
use AppSiteSettings;

class PostgreDB extends EngineDB
{
	private $_trans_started = false;

	private $_db_link;

	public function _open($db_name = AppSiteSettings::DB_NAME, DBParams $params = null)
	{
		$this->_settings($db_name, $params);
		if (!$this->_db_name) {
			throw new Exception('set up dbname!');
		}
		$shown = AppSiteSettings::DEBUG;
		if (!@$_SERVER['IDE_PHPUNIT_VERSION']) {
			$shown = ini_get('display_errors');
			ob_start();
			if (!$shown) {
				error_reporting(E_ALL);
				ini_set('display_errors', true);
			}
		}
		$this->_db_link = pg_connect('host=' . $this->_db_host . ' port=5432 dbname=' . $this->_db_name
									. ' user=' . $this->_db_user . ' password=' . $this->_db_pass);
		if (!@$_SERVER['IDE_PHPUNIT_VERSION']) {
			$message = ob_get_contents();
			if (!$shown) {
				error_reporting(0);
				ini_set('display_errors', false);
			}
			ob_end_clean();
		} else {
			$message = 'testing';
		}

		if (!$this->_db_link) {
			throw new Exception($message, VEG_ERROR_SYSTEM_DB);
		}
	}

	public function _close()
	{
		if (!$this->_db_link) {
			return false;
		}
		if (!(get_resource_type($this->_db_link) === 'pgsql link')) {
			throw new Exception ('unknown db link ' . $this->_db_name);
		}
		pg_close($this->_db_link);
		return true;
	}

	public function _closeLink($link)
	{
		if (!(get_resource_type($link) === 'pgsql link')) {
			return false;
		}
		return true; //dont break connection
	}

	public function _escape($str)
	{
		return pg_escape_string(trim($str));
	}

	public function _begin()
	{
		if (!$this->_db_link) {
			throw new Exception('DB TRANSACTION BEGIN NO LINK', VEG_ERROR_SYSTEM_DB);
		}
		if ($this->_trans_started) {
			throw new Exception('DB TRANSACTION STARTED', VEG_ERROR_SYSTEM_DB);
		}
		$this->_trans_started = true;
		pg_query($this->_db_link, 'BEGIN WORK');
	}

	public function _commit()
	{
		if (!$this->_db_link) {
			throw new Exception('DB TRANSACTION COMMIT NO LINK', VEG_ERROR_SYSTEM_DB);
		}
		if (!$this->_trans_started) {
			throw new Exception('DB TRANSACTION NOT STARTED', VEG_ERROR_SYSTEM_DB);
		}
		$this->_trans_started = false;
		pg_query($this->_db_link, 'COMMIT');
	}

	public function _rollback()
	{
		if (!$this->_db_link) {
			throw new Exception('DB TRANSACTION ROLLBACK NO LINK', VEG_ERROR_SYSTEM_DB);
		}
		if (!$this->_trans_started) {
			throw new Exception('DB TRANSACTION NOT STARTED', VEG_ERROR_SYSTEM_DB);
		}
		$this->_trans_started = false;
		pg_query($this->_db_link, 'ROLLBACK');
	}

	public function _query($query)
	{
		try {
			if (!$query) {
				return true;
			}
			$result = @pg_query($this->_db_link, $query);
			if (!$result) {
				$error = @pg_last_error($this->_db_link);
				throw new Exception('DB connection: ' . $this->_db_name . '<br/>' . PHP_EOL . 'DB error:' . $error . '<br/>' . PHP_EOL . $query);
			}
			return $result;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function _error()
	{
		return pg_errormessage($this->_db_link);
	}

	public function _numRows($link)
	{
		if (!is_object($link) && !is_resource($link)) {
			return '?';
		}
		return (int)pg_numrows($link);
	}

	public function _fetchAll($link)
	{
		return pg_fetch_all($link);
	}

	public function _fetchArray($link)
	{
		return pg_fetch_assoc($link);
	}

	public function _fetchRow($link)
	{
		return pg_fetch_row($link);
	}
	public function _truncate($table)
	{
		return $this->_query('TRUNCATE TABLE ' . DBHelper::escape($table) . ' RESTART IDENTITY');
	}
}
