<?php

namespace vegcoders\core\db;

use vegcoders\core\cache\Cache;
use AppEnginesSettings;
use AppSiteSettings;
class Dict
{
	private static $instance;
	static public function query($key, $sql, $return = DB::RETURN_LIST) {
		if (self::$instance === null){
			$c = __CLASS__;
			self::$instance = new $c;
		}
		$cache = Cache::get('dict_' . AppSiteSettings::VER_NUMBER . '_' . $key);
		if (!($cache === false)) {
			return $cache;
		}
		$cache = DB::query($sql, $return);
		Cache::put('dict_' . AppSiteSettings::VER_NUMBER . '_' . $key, $cache, true);
		return $cache;
	}
	static public function remove($key) {
		return Cache::remove('dict_' . AppSiteSettings::VER_NUMBER . '_' . $key);
	}
	static public function prep($id, $tmps) {
		if ($id === false) {
			return $tmps;
		}

		if (is_array($id)) {
			$tmps_2 = array();
			foreach ($id AS $i) {
				$tmps_2[$i] = @$tmps[$i];
			}
			return $tmps_2;
		}
		$ids = explode(',', $id);
		if (count($ids) === 1) {
			return @$tmps[$id];
		}
		$titles = array();
		foreach ($ids AS $i) {
			if (isset($tmps[$i])) {
				$titles[] = $tmps[$i];
			}
		}
		if (!$titles) {
			return '';
		}
		sort($titles);
		return implode(' | ', $titles);
	}

	/** for example
	 * @param bool|int $id
	 * @return array|string
	 */
	static public function blocks($id = false) {
		$tmps = Dict::query('content_blocks', 'SELECT * FROM ' . AppEnginesSettings::DB_TABLE_CONTENT_BLOCKS, 'alist');
		return Dict::prep($id, $tmps);
	}
}
