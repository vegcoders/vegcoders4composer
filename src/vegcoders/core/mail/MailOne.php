<?php
namespace vegcoders\core\mail;


class MailOne
{
	public $to;
	public $title;
	public $message;
	public $from = false;

	public function __construct($to, $title, $message)
	{
		$this->to = $to;
		if (!is_array($to)) {
			$this->to = explode(';', $to);
		}
		$this->title = $title;
		$this->message = $message;
	}
	public function setFrom($from)
	{
		$this->from = $from;
	}

	/**
	 * построение ключа для отчетов
	 * @return string
	 */
	public function getKey()
	{
		$key = implode(';', $this->to) . ' : ' . $this->title;
		if ($this->from) {
			$key = $this->from . ' -> ' . $key;
		}
		return $key;
	}
}