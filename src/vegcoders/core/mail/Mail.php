<?php
namespace vegcoders\core\mail;

use vegcoders\core\basic\BasicSingleLogged;
use vegcoders\core\mail\engines\EngineMailInterface;
use AppEnginesSettings;
/**
 * Класс единой рассылки разные подключения через "двигатель"
 */
class Mail extends BasicSingleLogged
{
	protected static $_instance;
	protected static $_can_debug;
	protected static $_can_debug_collect;
	protected $_mail_engine = array();

	public function getEngine($mail_class = AppEnginesSettings::CORE_MAIL_ENGINE) {
		if (!$mail_class) {
			$mail_class = AppEnginesSettings::CORE_MAIL_ENGINE;
		}
		if (!isset($this->_mail_engine[$mail_class])) {
			self::_log('engine:' .  $mail_class . '<br/>');
			$mail_class_full = 'vegcoders\\core\\mail\\engines\\' . $mail_class;
			$this->_mail_engine[$mail_class] = new $mail_class_full();
		}
		return $this->_mail_engine[$mail_class];
	}

	static public function send(MailOne $mailOne, $engine = false)
	{

		/** @var Mail $mail */
		$mail = self::getInstance();
		$key = $mailOne->getKey();
		self::_log('<div style="color:blue"><b>' . $key . '</b> put to mailing</div><br/>');

		/** @var EngineMailInterface $mailer */
		$mailer = $mail->getEngine($engine);
		$ok = $mailer->send($mailOne);
		if ($ok) {
			self::_log('<div style="color:green"><b>' . $key . '</b> sent to engine</div><br/>');
		} else {
			self::_log('<div style="color:red"><b>' . $key . '</b> send to engine error</div><br/>');
		}
		return $ok;
	}
}