<?php
namespace vegcoders\core\mail\engines;

use vegcoders\core\mail\MailOne;

class UsualMail implements EngineMailInterface
{
	/**
	 * @param MailOne $mailOne
	 * @return array results (to and success mark)
	 */
	static public function send(MailOne $mailOne)
	{
		$headers = "MIME-Version: 1.0\nContent-Type: text/html; charset=utf-8\nContent-Transfer-Encoding: 8bit";
		$results = array();
		foreach ($mailOne->to AS $to) {
			if ($mailOne->from) {
				$ok = mail($to, $mailOne->title, $mailOne->message, $headers . "\nFrom: " . $mailOne->from, '-f' . $mailOne->from);
			} else {
				$ok = mail($to, $mailOne->title, $mailOne->message, $headers);
			}
			$results[$to] = $ok;
		}
		return $results;
	}
}
