<?php
namespace vegcoders\core\mail\engines;

use vegcoders\core\mail\MailOne;

interface EngineMailInterface {
	static public function send(MailOne $mailOne);
}