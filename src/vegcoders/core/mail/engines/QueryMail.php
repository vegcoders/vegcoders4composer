<?php
namespace vegcoders\core\mail\engines;

use vegcoders\core\mail\MailOne;
use vegcoders\core\db\DB;
use AppEnginesSettings;
class QueryMail implements EngineMailInterface
{
	/**
	 * @param MailOne $mailOne
	 * @return array results (to and success mark)
	 */
	static public function send(MailOne $mailOne)
	{
		$results = array();
		$to_insert = array();
		foreach ($mailOne->to AS $to) {
			$to_insert = array();
			$to_insert[] = array(
				'from_email'  	=> $mailOne->from,
				'to_email'    	=> $to,
				'title'     	=> $mailOne->title,
				'message' 		=> $mailOne->message,
				'crtime'      	=> 'NOW()',
				'status'      	=> 0
			);
		}
		DB::insertMulti(AppEnginesSettings::CORE_MAIL_QUERY_DB_TABLE, $to_insert);

		return $results;
	}
}
