<?php
namespace vegcoders\core\dborm;

use vegcoders\core\db\DB;

class DBOrm
{
	protected $class_to_map = '';
	protected $table = '';


	protected $default_fields = array();
	protected $default_conditions = false;
	protected $default_order_by_field = false;
	protected $default_order_by_direction = false;
	protected $default_limit = false;

	private $current_fields = array();
	private $current_conditions = false;
	private $current_order_by_field = false;
	private $current_order_by_direction = false;
	private $current_limit = false;

	private $class_fields = array();

	public function loadAll()
	{
		$result = DB::queryArray($this->buildQuery());
		$this->resetCurrentValues();
		if (!$result) {
			return array();
		}
		$data = array();
		foreach ($result AS $row) {
			$data[] = new $this->class_to_map($row);
		}
		return $data;
	}

	public function loadOne()
	{
		$this->current_limit = 1;
		$result = DB::query($this->buildQuery(), DB::RETURN_ONE);
		$this->resetCurrentValues();
		if (!$result) {
			return false;
		}
		return new $this->class_to_map($result);
	}

	public function setFields($fields)
	{
		if (!$this->class_fields) {
			$this->buildClassFields();
		}
		if (!is_array($fields)) {
			$fields = explode(',', $fields);
		}
		$this->current_fields = array();
		foreach ($fields AS $field) {
			$field = trim($field);
			if (isset($this->class_fields[$field])) {
				$this->current_fields[$field] = $field;
			}
		}
		return $this;
	}

	public function setWhere($conditions)
	{
		/** @noinspection ArrayCastingEquivalentInspection */
		if (!is_array($conditions)) {
			$conditions = array($conditions);
		}
		$this->current_conditions = $conditions;
		return $this;
	}

	public function setOrderBy($field, $direction)
	{
		$this->current_order_by_field = $field;
		$this->current_order_by_direction = strtolower($direction) === 'desc' ? 'desc' : 'asc';
		return $this;
	}

	private function buildQuery()
	{
		$fields 	= $this->getQueryFields();
		$conditions = $this->getQueryConditions();
		$order_by 	=  $this->getQueryOrderBy();
		$limit 		=  $this->getQueryLimit();

		$query = 'SELECT ';
		$query .= implode(',', $fields);
		$query .= ' FROM ' . $this->table;
		if ($conditions) {
			$query .= ' WHERE ' . implode(' AND ', $conditions);
		}
		if ($order_by) {
			$query .= ' ORDER BY ' . $order_by;
		}
		if ($limit) {
			$query .= ' LIMIT ' . $limit;
		}
		return $query;
	}

	protected function getQueryFields()
	{
		if ($this->current_fields) {
			return $this->current_fields;
		}
		if ($this->default_fields) {
			return $this->default_fields;
		}
		if (!$this->class_fields) {
			$this->buildClassFields();
		}
		return $this->class_fields;
	}

	/**
	 * @return array
	 */
	protected function getQueryConditions()
	{
		$result = array();
		if ($this->current_conditions) {
			$result = $this->current_conditions;
		} elseif ($this->default_conditions) {
			$result = $this->default_conditions;
		}
		if (!$result) {
			return array();
		}
		return $result;
	}

	protected function getQueryOrderBy()
	{
		if ($this->current_order_by_field) {
			return $this->current_order_by_field . ' ' . $this->current_order_by_direction;
		}
		if ($this->default_order_by_field) {
			return $this->default_order_by_field . ' ' . $this->default_order_by_direction;
		}
		return false;
	}

	protected function getQueryLimit()
	{
		if ($this->current_limit) {
			return $this->current_limit;
		}
		if ($this->default_limit) {
			return $this->default_limit;
		}
		return false;
	}

	private function resetCurrentValues()
	{
		$this->current_fields = array();
		$this->current_conditions = false;
		$this->current_order_by_field = false;
		$this->current_order_by_direction = false;
		$this->current_limit = false;
	}

	private function buildClassFields()
	{
		$vars = get_class_vars($this->class_to_map);
		foreach ($vars AS $var => $params) {
			$this->class_fields[$var] = $var;
		}
	}
}