<?php
namespace vegcoders\db_tables\logs;

use vegcoders\core\db\DB;
use AppEnginesSettings;
class Debug
{
	public $crtime;
	public $title;
	public $text;
	public $text2;

	/**
	 * Debug constructor.
	 * @param $params = array('title' => 'title msg', 'text' => 'some text', 'text2' => 'other text')
	 */
	public function __construct($params = array())
	{
		$this->crtime = date('Y-m-d H:i:s');
		$this->title = isset($params['title']) ? $params['title'] : 'Error';
		$this->text = isset($params['text']) ? $params['text'] : '';
		$this->text2 = isset($params['text2']) ? $params['text2'] : '';
	}

	public function insert()
	{
		if (!AppEnginesSettings::DB_TABLE_LOGS_DEBUG) {
			return true;
		}
		return DB::insert(AppEnginesSettings::DB_TABLE_LOGS_DEBUG, array(
			'crtime' 		=> $this->crtime,
			'title' 		=> $this->title,
			'text' 			=> $this->text,
			'text2' 		=> $this->text2
		));
	}
}