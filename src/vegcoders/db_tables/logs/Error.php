<?php
namespace vegcoders\db_tables\logs;

use vegcoders\core\db\DB;
use vegcoders\core\funcs\F;
use AppEnginesSettings;

class Error
{
	public $crtime;
	public $code;
	public $title;
	public $source;
	public $description;

	/**
	 * Error constructor.
	 * @param $params = array('code' => 1, 'title' => 'title msg', 'source' => 'file name', 'description' => 'full desc'
	 */
	public function __construct($params = array())
	{
		$this->crtime = date('Y-m-d H:i:s');
		$this->code = isset($params['code']) ? (int) $params['code'] : VEG_ERROR_USER_GENERAL;
		$this->title = isset($params['title']) ? $params['title'] : 'Error';
		$this->source = isset($params['source']) ? $params['source'] : '';
		$this->description = isset($params['description']) ? $params['description'] : '';
	}

	public function insert()
	{
		return DB::insert(AppEnginesSettings::DB_TABLE_LOGS_ERRORS, array(
			'crtime' => $this->crtime,
			'code' => $this->code,
			'title' => $this->title,
			'source' => $this->source,
			'description' => $this->description,
			'from_url' => F::referer(),
			'ip' => F::ip(),
			'uri' => F::uri(),
			'agent' => F::agent()
		));
	}
}