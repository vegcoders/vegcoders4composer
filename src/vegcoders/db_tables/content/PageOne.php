<?php
namespace vegcoders\db_tables\content;

use vegcoders\core\dborm\DBOrmOne;

class PageOne extends DBOrmOne {
	public $id;
	public $crtime;
	public $title;
	public $text;
}