<?php
namespace vegcoders\db_tables\content;

use AppEnginesSettings;
use vegcoders\core\dborm\DBOrm;

class Pages extends DBOrm
{
	public function __construct()
	{
		$this->class_to_map = 'vegcoders\\db_tables\\content\\PageOne';
		$this->table = AppEnginesSettings::DB_TABLE_CONTENT_PAGES;
	}

	protected function getQueryConditions()
	{
		$parent = parent::getQueryConditions();
		$parent['status'] = 'status=1';
		return $parent;
	}
}