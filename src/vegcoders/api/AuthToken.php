<?php
/**
 * генерация токенов для авторизации
 * @todo при оптимизации - можно сохранять уже сгенеренные в memcache, каждый раз зная что при потере кеша мы можем запросто проверить
 */
namespace vegcoders\api;

use AppEnginesSettings;

class AuthToken
{
	private static $key = AppEnginesSettings::API_KEY;
	private static $iv = AppEnginesSettings::API_KEY_AFTER;

	public static function create($id, $expire = 0, $mode = 0, $now = false)
	{
		$id = intval($id);
		$expire = intval($expire);
		$mode = intval($mode);
		if ($id < 0 || $expire < 0 || $mode < 0) {
			return null;
		}
		$info = array();
		$info["id"] = $id;
		$info["time"] = $now ? $now : time(); //for tests run with some other time
		$info["expire"] = $expire;
		$info["mode"] = $mode;
		$info["rnd"] = ceil(mt_rand(0, 255)); //длинна от степени двойки зависит - 1023 уходит в другую разрядность));
		$info["sum"] = self::_sum($info);
		//echo '<span style="color:blue">' . var_export($info, true) . '</span><br/>';

		$info = self::int2char($info["id"])
			. self::int2char($info["time"])
			. self::int2char($info["expire"])
			. chr($info["mode"])
			. chr($info["rnd"]) //или вот тут удлиннять!
			. self::int2char($info["sum"]);
		$token = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(self::$key), $info, MCRYPT_MODE_OFB, md5(self::$iv));
		$tokenHex = "";
		$tokenLength = strlen($token);
		for ($i = 0; $i < $tokenLength; $i++) {
			$tokenHex .= sprintf("%02x", ord($token{$i}));
		}
		return $tokenHex;
	}

	public static function check($tokenHex, $mode = null)
	{
		$token = "";
		$tokenHexLength = strlen($tokenHex) / 2;
		for ($i = 0; $i < $tokenHexLength; $i++) {
			$token .= chr(hexdec(substr($tokenHex, $i * 2, 2)));
		}
		$info = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(self::$key), $token, MCRYPT_MODE_OFB, md5(self::$iv));
		if (!(strlen($info) == 18)) {
			return false;
		}
		$info = array(
			"id" => self::char2int(substr($info, 0, 4)),
			"time" => self::char2int(substr($info, 4, 4)),
			"expire" => self::char2int(substr($info, 8, 4)),
			"mode" => ord($info{12}),
			"rnd" => ord($info{13}), // и соответсвенно тут удлиннять если мы лимит рендома увеличим
			"sum" => self::char2int(substr($info, 14, 4))
		);
		//echo '<span style="color:purple">' . var_export($info, true) . '</span><br/>';
		if (self::_sum($info) != $info['sum']) {
			return -1 * __LINE__;
		}
		if (($info["expire"] > 0) && ($info["expire"] + $info["time"] < time())) {
			return -1 * __LINE__;
		}
		if ($info["mode"] > 0 && $mode !== null && $info["mode"] != $mode) {
			return -1 * __LINE__;
		}
		return $info["id"];
	}

	private static function _sum($info)
	{
		return $info["time"] - $info["expire"] - $info["mode"] - $info["rnd"] - $info["id"];
	}

	private static function int2char($int)
	{
		$char = "";
		$hex = sprintf("%08x", $int);
		for ($i = 0; $i < 4; $i++) {
			$char .= chr(hexdec(substr($hex, $i * 2, 2)));
		}
		return $char;
	}

	private static function char2int($char)
	{
		$int = 0;
		$hex = "";
		for ($i = 0; $i < 4; $i++) {
			$hex .= sprintf("%02x", ord($char{$i}));
		}
		$int = hexdec($hex);
		return $int;
	}
}