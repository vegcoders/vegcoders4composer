<?php

/** @noinspection PhpIllegalPsrClassPathInspection */
class VegSiteSettings
{
	const FRONT_SITE_URL = '';
	const ROOT_PATH = __DIR__ . '/../';
	const TMP_PATH = __DIR__ . '/../tmp/';
	const PUBLIC_PATH = __DIR__ . '/../public/';


	const DB_HOST = 'localhost';
	const DB_USER = 'postgres';
	const DB_PASS = '111';
	const DB_NAME = 'turtus_test';
	
	const DATE_FORMAT = 'Y-m-d';
	
	const DEBUG = true;
	const VER_NUMBER = '1';
	
	
}