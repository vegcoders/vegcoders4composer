<?php
if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

/* различные типы параметров для валидации через FV::paramsCheck или базовой F::get*/
define('VEG_PARAM_INT', 		'int');
define('VEG_PARAM_INT_INCLUDE_ZERO', 'intzero');
define('VEG_PARAM_INTS', 		'intarray');	//массив циферок (или через запятую)
define('VEG_PARAM_ARRAY', 		'array');		//массив
define('VEG_PARAM_NUMERIC', 	'numeric');
define('VEG_PARAM_DATETIME', 	'datetime');
define('VEG_PARAM_DATE', 		'date');
define('VEG_PARAM_TIME', 		'time');
define('VEG_PARAM_STRING', 		'string');
define('VEG_PARAM_EMAIL', 		'email');
define('VEG_PARAM_INARRAY', 	'inarray');
define('VEG_PARAM_EDIT', 	    'edit');
define('VEG_PARAM_PHONE', 	    'phone');
define('VEG_PARAM_TEXTAREA', 	'textarea');
define('VEG_PARAM_YES_NO', 		'yes_no');
define('VEG_PARAM_JSON', 		'json');


define('VEG_ERROR_SYSTEM_PARAMETER_INVALID', -600);
define('VEG_ERROR_SYSTEM_DB', -900);
define('VEG_ERROR_SYSTEM_GENERAL', -1000);
define('VEG_ERROR_USER_GENERAL', 1000);
